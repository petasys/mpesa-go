package mpesa

import (
	"net/http"
	"sync"
	"time"
)

var client Client
var clientLock = &sync.Mutex{}

type Client interface {
	Post(*http.Request) (*http.Response, error)
	Get(*http.Request) (*http.Response, error)
}

type Config struct {

	// WriteReadTimeout specifies a time limit for requests made by this
	// Client. The timeout includes connection time, any
	// redirects, and reading the response body. The timer remains
	// running after Get, Head, Post, or Do return and will
	// interrupt reading of the Response.Body.
	//
	// A WriteReadTimeout of zero means no timeout.
	//
	// The Client cancels requests to the underlying Transport
	// as if the Request's Context ended.
	//
	// For compatibility, the Client will also use the deprecated
	// CancelRequest method on Transport if found. New
	// RoundTripper implementations should use the Request's Context
	// for cancellation instead of implementing CancelRequest.
	WriteReadTimeout time.Duration

	// MaxIdleConns controls the maximum number of idle (keep-alive)
	// connections across all hosts. Zero means no limit.
	MaxIdleConns int

	// MaxIdleConnsPerHost, if non-zero, controls the maximum idle
	// (keep-alive) connections to keep per-host. If zero,
	// DefaultMaxIdleConnsPerHost is used.
	MaxIdleConnsPerHost int

	// MaxConnsPerHost optionally limits the total number of
	// connections per host, including connections in the dialing,
	// active, and idle states. On limit violation, dials will block.
	//
	// Zero means no limit.
	MaxConnsPerHost int

	// IdleConnTimeout is the maximum amount of time an idle
	// (keep-alive) connection will remain idle before closing
	// itself.
	// Zero means no limit.
	IdleConnTimeout time.Duration
}

type defaultClient struct {
	client  *http.Client
	ceptors []Interceptor
}

func (c *defaultClient) Post(r *http.Request) (*http.Response, error) {
	r.Method = http.MethodPost
	return c.do(r)
}

func (c *defaultClient) Get(r *http.Request) (*http.Response, error) {
	r.Method = http.MethodGet
	return c.do(r)
}

func (c *defaultClient) do(r *http.Request) (*http.Response, error) {
	r.Header.Set("User-Agent", LibraryName)

	chain := defaultChain{ceptors: c.ceptors, client: c.client}
	return chain.Proceed(r)
}

func DefaultConfig() *Config {
	return &Config{
		WriteReadTimeout:    time.Second * 30,
		MaxIdleConns:        1,
		MaxIdleConnsPerHost: 1,
		MaxConnsPerHost:     200,
		IdleConnTimeout:     time.Minute * 30,
	}
}

func ClientInstance(cfg *Config, ceptors []Interceptor) Client {
	if client == nil {
		clientLock.Lock()
		defer clientLock.Unlock()

		t := &http.Transport{
			MaxIdleConnsPerHost: cfg.MaxIdleConnsPerHost,
			MaxIdleConns:        cfg.MaxIdleConns,
			MaxConnsPerHost:     cfg.MaxConnsPerHost,
			IdleConnTimeout:     cfg.IdleConnTimeout,
		}

		c := &http.Client{Transport: t, Timeout: cfg.WriteReadTimeout}
		client = &defaultClient{client: c, ceptors: ceptors}
	}

	return client
}
