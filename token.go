package mpesa

import (
	"errors"
	"fmt"
	"time"
)

// Token holds the OAuth access token and the expiry info returned by the OAuth
// M-Pesa API.
type Token interface {

	// Shortcode returns the unique number that is allocated to a pay bill or buy
	// goods organization through they will be able to receive customer payment.
	//
	// It could be a Pay bill, Buy Goods or Till Number.
	Shortcode() string

	// Token returns the OAuth Access token returned from M-Pesa.
	//
	// This access token is used to access other M-Pesa APIs.
	Token() string

	// Expired returns true if the access token has expired, false, otherwise.
	Expired() bool

	// ExpiresIn returns the access token expiry time in seconds.
	ExpiresIn() time.Duration

	// ExpiryTime returns the precise date and time the access token is expected
	// to have expired.
	ExpiryTime() time.Time
}

type TokenStore interface {
	Read(string) (Token, error)
	ReadAll() ([]Token, error)
	ReadExpired() ([]Token, error)
	Write(shortcode string, tkn string, exp time.Duration) error
	Delete(string) error
}

type defaultToken struct {
	shortcode  string
	token      string
	expiryTime time.Time
	expiresIn  time.Duration
}

type InmemoryTokenStore struct {
	tokens map[string]Token
}

func (t *defaultToken) Shortcode() string {
	return t.shortcode
}

func (t *defaultToken) SetShortcode(shortcode string) {
	t.shortcode = shortcode
}

func (t *defaultToken) Token() string {
	return t.token
}

func (t *defaultToken) Expired() bool {
	return time.Now().After(t.expiryTime)
}

func (t *defaultToken) ExpiresIn() time.Duration {
	return t.expiresIn
}

func (t *defaultToken) ExpiryTime() time.Time {
	return t.expiryTime
}

func (t *defaultToken) String() string {
	return fmt.Sprintln(t.shortcode, t.token, t.expiresIn, t.expiryTime)
}

func (s *InmemoryTokenStore) Read(shortcode string) (Token, error) {
	t, ok := s.tokens[shortcode]
	if !ok {
		return nil, errors.New("access token not found")
	}

	return t, nil
}

func (s *InmemoryTokenStore) ReadAll() ([]Token, error) {
	tokens := make([]Token, 0, len(s.tokens))
	for _, v := range s.tokens {
		tokens = append(tokens, v)
	}

	return tokens, nil
}

func (s *InmemoryTokenStore) ReadExpired() ([]Token, error) {
	tokens := make([]Token, 0, len(s.tokens))
	for _, v := range s.tokens {
		if v.Expired() {
			tokens = append(tokens, v)
		}
	}

	return tokens, nil
}

func (s *InmemoryTokenStore) Write(shortcode string, tkn string, exp time.Duration) error {
	if len(shortcode) == 0 {
		return errors.New("shortcode is blank")
	}

	if len(tkn) == 0 {
		return errors.New("token is blank")
	}

	expiryTime := time.Now().Add(exp - time.Minute)
	token := defaultToken{shortcode: shortcode, token: tkn, expiresIn: exp, expiryTime: expiryTime}
	s.tokens[shortcode] = &token
	return nil
}

func (s *InmemoryTokenStore) Delete(shortcode string) error {
	delete(s.tokens, shortcode)
	return nil
}

func NewInmemoryTokenStore() *InmemoryTokenStore {
	return &InmemoryTokenStore{tokens: map[string]Token{}}
}
