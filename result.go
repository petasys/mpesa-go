package mpesa

import (
	"errors"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

const (

	// The service request is processed successfully.
	ResultCodeSuccess = 0

	// The balance is insufficient for the transaction.
	ResultCodeInsufficientBalance = 1

	// Declined due to limit rule
	ResultCodeDeclinedDueToLimitRule = 2

	// The initiator information is invalid.
	ResultCodeInvalidInitiatorInformation = 2001

	// Declined due to account rule: The account status does not allow this transaction.
	ResultCodeAccountStatusDoesNotAllowTxn = 2006

	// The transaction receipt number does not exist.
	ResultCodeReceiptNumberNotFound = 2032

	// The transaction receipt number cannot be found by the specified ConversationID
	// or OriginatorConversationID.
	ResultCodeConversationIDNotFound = 2033

	// Credit Party customer type (Unregistered or Registered Customer) can't be
	// supported by the service.
	ResultCodeCreditPartyCustomerTypeNotSupported = 2040

	// This organization is not a child organization of the initiator.
	ResultCodeOrgNotChildOrgOfTheInitiator = 2043
)

// {Amount={CurrencyCode=(?P<currency>\w+), MinimumAmount=(?P<minimum>\w+), BasicAmount=(?P<basic>\w+)}}
var AmountRegexp = regexp.MustCompile(`{Amount={CurrencyCode=(?P<currency>[A-Z]{3}), MinimumAmount=(?P<minimum>.+), BasicAmount=(?P<basic>.+)}}`)

type Kv struct {
	K string      `json:"Key"`
	V interface{} `json:"Value"`
}

// Amount holds an account balance and is usually returned by M-Pesa to you as
// part of a Transaction status query result, B2B result or B2C result.
//
// As of the writing of this code, amount always has the following format:
//
//	{Amount={CurrencyCode=KES, MinimumAmount=99200, BasicAmount=992.00}}.
//
// NB: This is however subject to change and depends entirely on M-Pesa.
type Amount struct {

	// Currency is the currency code of the account.
	//
	// Required.
	Currency string

	// Basic is the actual current account balance.
	//
	// Required.
	Basic float64

	// Minimum is a result of mulitplying the actual current account balance by
	// 100.
	//
	// NB: Do not use this value anywhere. From our experience (The Authors of M-Pesa Go),
	// this value does not show the actual current account balance.
	Minimum string
}

// Party represents an individual or an organization receiving or sending money
// on the M-Pesa platform.
type Party struct {

	// Name is the full name of the customer/recipient.
	Name string

	// Shortcode is the customer's phone number in the format 2547XXXXXX or the
	// organization's shortcode.
	Shortcode string
}

// Parameters is a JSON object that holds more details for the transaction.
type Parameters struct {

	// Parameter is a JSON array within the ResultParameters that holds
	// additional transaction details as JSON objects.
	//
	// This list of additional payment details is only present is the payment
	// was successful.
	//
	// Optional.
	Parameter []Kv `json:"ResultParameter"`
}

// Value returns the value associated with a certain parameter key.
func Value(k string, params []Kv) (interface{}, error) {
	if len(params) == 0 {
		return nil, fmt.Errorf("ResultParameter %s not found. ResultParameter list is either Nil or empty", k)
	}

	for _, kv := range params {
		if kv.K == k {
			return kv.V, nil
		}
	}

	return nil, fmt.Errorf("ResultParameter %s not found", k)
}

// InvalidParameterValueError returns a general validation error for result parameters
// whose values do not match the expected type.
func InvalidParameterValueError(k string, v interface{}) error {
	return fmt.Errorf("%v is an invalid %s result parameter value", v, k)
}

func NewParty(v string) (*Party, bool) {
	data := strings.Split(v, "-")
	if len(data) != 2 {
		return nil, false
	}

	shortcode := strings.TrimSpace(data[0])
	name := strings.TrimSpace(data[1])
	return &Party{Name: name, Shortcode: shortcode}, true
}

func NewAmount(v string) (*Amount, error) {
	matches := AmountRegexp.FindStringSubmatch(v)
	if len(matches) == 0 {
		return nil, errors.New(v + " is an invalid Amount")
	}

	ccyIndex := AmountRegexp.SubexpIndex("currency")
	minIndex := AmountRegexp.SubexpIndex("minimum")
	basicIndex := AmountRegexp.SubexpIndex("basic")

	basic, err := strconv.ParseFloat(matches[basicIndex], 64)
	if err != nil {
		return nil, errors.New(matches[basicIndex] + " is an invalid BasicAmount")
	}

	return &Amount{Currency: matches[ccyIndex], Basic: basic, Minimum: matches[minIndex]}, nil
}

// String converts Recipient to text.
func (r *Party) String() string {
	return fmt.Sprintf("%s - %s", r.Shortcode, r.Name)
}

// String converts Amount to text.
func (a *Amount) String() string {
	return fmt.Sprintf("{Amount={CurrencyCode=%s, MinimumAmount=%s, BasicAmount=%f}}", a.Currency, a.Minimum, a.Basic)
}
