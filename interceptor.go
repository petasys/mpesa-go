package mpesa

import (
	"net/http"
)

type Chain interface {
	Pos() int
	Proceed(*http.Request) (*http.Response, error)
}

type Interceptor interface {
	Matcher() Matcher
	Intercept(Chain, *http.Request) (*http.Response, error)
}

type defaultChain struct {
	pos     int
	ceptors []Interceptor
	client  *http.Client
}

func (c *defaultChain) Pos() int {
	return c.pos
}

func (c *defaultChain) Proceed(r *http.Request) (*http.Response, error) {
	if len(c.ceptors) == 0 || c.pos >= len(c.ceptors) {
		return c.client.Do(r)
	}

	ceptor := c.ceptors[c.pos]
	c.pos++

	if match := ceptor.Matcher(); match != nil && match(r) {
		return ceptor.Intercept(c, r)
	}

	return c.Proceed(r)
}
