package express

// Request is the HTTP request body for the M-Pesa Express API.
type Request struct {

	// Shortcode is the organization's shortcode (Paybill or Buygoods - A 5 to 6-digit account number)
	// used to identify an organization and receive the transaction.
	//
	// Required.
	Shortcode string `json:"BusinessShortCode" validate:""`

	// Password is the password used for encrypting the request sent: A base64
	// encoded string. (The base64 string is a combination of Shortcode+Passkey+Timestamp)
	//
	// Required.
	Password string `json:"Password" validate:"base64"`

	// Timestamp is the  is the Timestamp of the transaction, normally in the format
	// of YEAR+MONTH+DATE+HOUR+MINUTE+SECOND (YYYYMMDDHHMMSS) Each part should be at
	// least two digits apart from the year which takes four digits.
	//
	// Required.
	Timestamp string `json:"Timestamp" validate:"required"`

	// TxnType is the transaction type that is used to identify the transaction
	// when sending the request to M-PESA. The transaction type for M-PESA Express
	// is "CustomerPayBillOnline" for PayBill Numbers and "CustomerBuyGoodsOnline"
	// for Till Numbers.
	//
	// Required.
	TxnType string `json:"TransactionType" validate:"oneof=CustomerBuyGoodsOnline CustomerPayBillOnline"`

	// Amount is the Amount transacted normally a numeric value. Money that the
	// customer pays to the Shortcode.
	//
	// Only whole numbers are supported.
	//
	// Required.
	Amount float64 `json:"Amount" validate:"min=1,max=150000"`

	// PartyA is the phone number sending money.
	//
	// The parameter expected is a Valid Safaricom Mobile Number that is M-PESA
	// registered in the format 2547XXXXXXXX.
	//
	// Required.
	PartyA string `json:"PartyA" validate:"startswith=254,numeric,len=12"`

	// PartyB is the organization that receives the funds. The parameter expected
	// is a 5 to 6-digit as defined in the Shortcode description above.
	//
	// This can be the same as the ShortCode value above.
	//
	// Required.
	PartyB string `json:"PartyB" validate:"numeric,min=5,max=16"`

	// PhoneNumber is the Mobile Number to receive the STK Pin Prompt.
	//
	// This number can be the same as PartyA value above.
	//
	// Required.
	PhoneNumber string `json:"PhoneNumber" validate:"startswith=254,numeric,len=12"`

	// CallbackURL is a valid secure URL that is used to receive notifications
	// from M-Pesa API.
	//
	// It is the endpoint to which the results will be sent by M-Pesa API.
	//
	// Required.
	CallBackURL string `json:"CallBackURL" validate:"http_url"`

	// Reference is  an alpha-numeric parameter that is defined by your system
	// as an Identifier of the transaction for the CustomerPayBillOnline transaction type.
	//
	// Along with the business name, this value is also displayed to the customer
	// in the STK Pin Prompt message.
	//
	// Maximum of 12 characters.
	//
	// Required.
	Reference string `json:"AccountReference" validate:"alphanum,max=12"`

	// TxnDesc is any additional information/comment that can be sent along with
	// the request from your system.
	//
	// Maximum of 13 Characters.
	//
	// Optional.
	TxnDesc string `json:"TransactionDesc" validate:"max=13"`
}

// OnlineBuyGoodsInfo is a set of payment details that are required for a customer buy goods
// online transaction.
//
// The rest of the payment details are sourced from respective stores e.g. TokenStore, and
// automatically added to the final HTTP request body (Request).
type OnlineBuyGoodsInfo struct {

	// Amount is the Amount transacted normally a numeric value. Money that the
	// customer pays to the Shortcode.
	//
	// Only whole numbers are supported.
	//
	// Required.
	Amount float64 `json:"Amount" validate:"min=1,max=150000"`

	// PartyB is the organization that receives the funds. The parameter expected
	// is a 5 to 6-digit as defined in the Shortcode description above.
	//
	// This is a Till number.
	//
	// Required.
	PartyB string `json:"PartyB" validate:"numeric,min=5,max=16"`

	// PhoneNumber is the Mobile Number to receive the STK Pin Prompt.
	//
	// This number can be the same as PartyA value above.
	//
	// Required.
	PhoneNumber string `json:"PhoneNumber" validate:"startswith=254,numeric,len=12"`

	// CallbackURL is a valid secure URL that is used to receive notifications
	// from M-Pesa API.
	//
	// It is the endpoint to which the results will be sent by M-Pesa API.
	//
	// Required.
	CallBackURL string `json:"CallBackURL" validate:"http_url"`

	// Reference is  an alpha-numeric parameter that is defined by your system
	// as an Identifier of the transaction for the CustomerPayBillOnline transaction type.
	//
	// Along with the business name, this value is also displayed to the customer
	// in the STK Pin Prompt message.
	//
	// Maximum of 12 characters.
	//
	// Required.
	Reference string `json:"AccountReference" validate:"alphanum,max=12"`

	// TxnDesc is any additional information/comment that can be sent along with
	// the request from your system.
	//
	// Maximum of 13 Characters.
	//
	// Required.
	TxnDesc string `json:"TransactionDesc" validate:"min=1,max=13"`
}

// OnlineBillpayInfo is a set of payment details that are required for a customer paybill
// online transaction.
//
// The rest of the payment details are sourced from respective stores e.g. TokenStore, and
// automatically added to the final HTTP request body (Request).
type OnlineBillpayInfo struct {

	// Amount is the Amount transacted normally a numeric value. Money that the
	// customer pays to the Shortcode.
	//
	// Only whole numbers are supported.
	//
	// Required.
	Amount float64 `json:"Amount" validate:"min=1,max=150000"`

	// PhoneNumber is the Mobile Number to receive the STK Pin Prompt.
	//
	// This number can be the same as PartyA value above.
	//
	// Required.
	PhoneNumber string `json:"PhoneNumber" validate:"startswith=254,numeric,len=12"`

	// CallbackURL is a valid secure URL that is used to receive notifications
	// from M-Pesa API.
	//
	// It is the endpoint to which the results will be sent by M-Pesa API.
	//
	// Required.
	CallBackURL string `json:"CallBackURL" validate:"http_url"`

	// Reference is  an alpha-numeric parameter that is defined by your system
	// as an Identifier of the transaction for the CustomerPayBillOnline transaction type.
	//
	// Along with the business name, this value is also displayed to the customer
	// in the STK Pin Prompt message.
	//
	// Maximum of 12 characters.
	//
	// Required.
	Reference string `json:"AccountReference" validate:"alphanum,max=12"`

	// TxnDesc is any additional information/comment that can be sent along with
	// the request from your system.
	//
	// Maximum of 13 Characters.
	//
	// Required.
	TxnDesc string `json:"TransactionDesc" validate:"min=1,max=13"`
}
