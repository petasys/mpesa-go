package express

type Query struct {

	// CRID (CheckoutRequestID) is a global unique identifier of the processed
	// checkout transaction request.
	//
	// Required.
	CRID string `json:"CheckoutRequestID" validate:"printascii,min=1,max=256"`

	// Shortcode is the organization's shortcode (Paybill or Buygoods - A 5 to 6-digit account number)
	// used to identify an organization and receive the transaction.
	//
	// Required.
	Shortcode string `json:"BusinessShortCode" validate:""`

	// Password is the password used for encrypting the request sent: A base64
	// encoded string. (The base64 string is a combination of Shortcode+Passkey+Timestamp)
	//
	// Required.
	Password string `json:"Password" validate:"base64"`

	// Timestamp is the  is the Timestamp of the transaction, normally in the format
	// of YEAR+MONTH+DATE+HOUR+MINUTE+SECOND (YYYYMMDDHHMMSS) Each part should be at
	// least two digits apart from the year which takes four digits.
	//
	// Required.
	Timestamp string `json:"Timestamp" validate:"required"`
}

// Txn is M-Pesa's STK push status query response.
type Txn struct {

	// MRID (MerchantRequestID) is a global unique Identifier for any submitted
	// payment request.
	//
	// Required.
	MRID string `json:"MerchantRequestID" validate:"printascii,min=1,max=256"`

	// CRID (CheckoutRequestID) is a global unique identifier of the processed
	// checkout transaction request.
	//
	// Required.
	CRID string `json:"CheckoutRequestID" validate:"printascii,min=1,max=256"`

	// ResponseCode (ResponseCode) is a Numeric status code that indicates the status of
	// the transaction submission.
	//
	// 0 means successful submission and any other code means an error occurred.
	//
	// Required.
	ResponseCode string `json:"ResponseCode" validate:"min=0"`

	// ResponseDesc (ResponseDescription) is an acknowledgment message from the API that
	// gives the status of the request submission.
	//
	// It usually maps to a specific ResponseCode value. It can be a Success submission
	// message or an error description.
	//
	// Required.
	ResponseDesc string `json:"ResponseDescription" validate:"printascii,max=512"`

	// ResultCode (ResultCode) is a numeric status code that indicates the status of the
	// transaction processing.
	//
	// 0 means successful processing and any other code means an error occurred
	// or the transaction failed.
	//
	// Required.
	ResultCode string `json:"ResultCode" validate:"min=0"`

	// Desc (ResultDesc) is a message from the API that gives the status
	// of the request processing.
	//
	// It usually maps to a specific ResultCode value.
	//
	// It can be a Success processing message or an error description message.
	//
	// Required.
	ResultDesc string `json:"ResultDesc" validate:"printascii,max=512"`
}

// Successful returns true if the request submission was successful, false, otherwise.
func (r *Txn) Successful() bool {
	return r.ResponseCode == "0"
}

// TxnSuccessful returns true if the online customer payment was successful, false,
// otherwise.
func (r *Txn) TxnSuccessful() bool {
	return r.ResultCode == "0"
}
