package express

import (
	"time"

	"gitlab.com/petasys/mpesa-go"
)

const transactionDateFormat = "20060102150405"

func parseTransactionDate(txnDate string) (time.Time, error) {
	return time.ParseInLocation(transactionDateFormat, txnDate, mpesa.EATZone)
}

func nowString() string {
	return time.Now().In(mpesa.EATZone).Format(transactionDateFormat)
}
