package express

// Response is an acknowledgement response body return by M-Pesa Express API on
// success.
type Response struct {

	// MRID (MerchantRequestID) is a global unique Identifier for any submitted
	// payment request.
	//
	// Required.
	MRID string `json:"MerchantRequestID" validate:"printascii,min=1,max=256"`

	// CRID (CheckoutRequestID) is a global unique identifier of the processed
	// checkout transaction request.
	//
	// Required.
	CRID string `json:"CheckoutRequestID" validate:"printascii,min=1,max=256"`

	// Code (ResponseCode) is a Numeric status code that indicates the status of
	// the transaction submission.
	//
	// 0 means successful submission and any other code means an error occurred.
	//
	// Required.
	Code string `json:"ResponseCode" validate:"min=0"`

	// Desc (ResponseDescription) is an acknowledgment message from the API that
	// gives the status of the request submission.
	//
	// It usually maps to a specific ResponseCode value. It can be a Success submission
	// message or an error description.
	//
	// Required.
	Desc string `json:"ResponseDescription" validate:"printascii,max=512"`

	// Message (CustomerMessage) is a message that your system can display to the
	// customer as an acknowledgment of the payment request submission.
	//
	// Optional.
	Message string `json:"CustomerMessage" validate:"printascii,max=512"`
}

// Successful returns true if the request submission was successful, false, otherwise.
func (r *Response) Successful() bool {
	return r.Code == "0"
}
