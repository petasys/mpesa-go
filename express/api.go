// Copyright 2023 M-Pesa Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Lipa na M-PESA online API also known as M-PESA express (STK Push/NI push) is
// a Merchant/Business initiated C2B (Customer to Business) Payment.
//
// Once you, our merchant integrate with the API, you will be able to send a
// payment prompt on the customer's phone (Popularly known as STK Push Prompt)
// to your customer's M-PESA registered phone number requesting them to enter
// their M-PESA pin to authorize and complete payment.
//
// This eliminates the challenge of having to remember business pay bill numbers
// and account numbers and allows customers to confirm the transaction by entering
// their M-PESA PIN on their mobile phones. For the business, this API enables
// them to preset all the correct info in the payment request and reduce the chances
// of wrong payments being performed to their systems.
//
// It is a C2B transaction, but the initiator is the organization instead of the customer.
//
// Since the organization has the option of presetting all required variables in the request
// before sending the request, this API has no Validation-Confirmation process like C2B API.
//
// STK push simply means sim tool kit initiated push, this has now evolved to NI push
// (network initiated push).
//
// The Lipa na M-PESA online API process is explained below;
//  1. The Merchant(Partner) captures and sets the API required parameters and
//     sends the API request.
//  2. The API receives the request and validates it internally first, then sends
//     you an acknowledgment response.
//  3. Through API Proxy an STK Push trigger request is sent to the M-PESA registered
//     phone number of the customer's making the payment.
//  4. The customer confirms by entering their M-PESA PIN.
//  5. The response is sent back to M-PESA and is processed as below:
//     a) M-PESA validates the customer's PIN
//     b) M-PESA debits the customer's Mobile Wallet.
//     c) M-PESA credits the Merchant (Partner) account.
//  6. Once the request is processed send the RESULTS back to the API Management
//     system which is then forwarded to the merchant via the callback URL specified in the REQUEST.
//  7. The customer receives an SMS confirmation message of the payment.
package express

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-playground/validator/v10"
	"gitlab.com/petasys/mpesa-go"
)

// API is a implementation of the 2 M-Pesa Express APIs: Paybill and Buy Goods online.
type API struct {

	// env is the Express API host name.
	env string

	// client is the underlying HTTP client with support for interceptors e.t.c.
	client mpesa.Client

	// tknStore is a store where the access token is retrived for each transaction.
	tknStore mpesa.TokenStore

	// appStore is a store where all registered Developer apps are held.
	//
	// We need to get the correct app for each shortcode because a Passkey is required
	// and can only be found in an App instance.
	appStore mpesa.AppStore

	// validate is used to run validation checks for all request params. The idea is to
	// cancel an invalid request before it makes it to M-Pesa.
	validate *validator.Validate
}

func (api *API) sendSTKPush(tkn mpesa.Token, b *Request) (*Response, error) {
	bBytes, err := json.Marshal(&b)
	if err != nil {
		return nil, err
	}

	url := api.env + mpesa.PathSTKPushProcess
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(bBytes))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", "Bearer "+tkn.Token())
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	res, err := api.client.Post(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		var errResBody mpesa.ErrorResponse
		if err := json.NewDecoder(res.Body).Decode(&errResBody); err != nil {
			return nil, err
		}

		return nil, &errResBody
	}

	var resBody Response
	if err := json.NewDecoder(res.Body).Decode(&resBody); err != nil {
		return nil, err
	}

	return &resBody, nil
}

func (api *API) BuyGoodsOnline(shortcode string, r *OnlineBuyGoodsInfo) (*Request, *Response, error) {
	if err := api.validate.Struct(r); err != nil {
		return nil, nil, err
	}

	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	app, err := api.appStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	tmstmp := nowString()
	pwd := shortcode + app.Passkey() + tmstmp
	pwd = base64.StdEncoding.EncodeToString([]byte(pwd))

	b := Request{
		Shortcode:   shortcode,
		Password:    pwd,
		Timestamp:   tmstmp,
		TxnType:     mpesa.TransactionTypeCustomerBuyGoodsOnline,
		Amount:      r.Amount,
		PartyA:      r.PhoneNumber,
		PartyB:      r.PartyB,
		PhoneNumber: r.PhoneNumber,
		CallBackURL: r.CallBackURL,
		Reference:   r.Reference,
		TxnDesc:     r.TxnDesc,
	}

	res, err := api.sendSTKPush(tkn, &b)
	return &b, res, err
}

func (api *API) PayBillOnline(shortcode string, r *OnlineBillpayInfo) (*Request, *Response, error) {
	if err := api.validate.Struct(r); err != nil {
		return nil, nil, err
	}

	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	app, err := api.appStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	tmstmp := nowString()
	pwd := shortcode + app.Passkey() + tmstmp
	pwd = base64.StdEncoding.EncodeToString([]byte(pwd))

	b := Request{
		Shortcode:   shortcode,
		Password:    pwd,
		Timestamp:   tmstmp,
		TxnType:     mpesa.TransactionTypeCustomerPayBillOnline,
		Amount:      r.Amount,
		PartyA:      r.PhoneNumber,
		PartyB:      shortcode,
		PhoneNumber: r.PhoneNumber,
		CallBackURL: r.CallBackURL,
		Reference:   r.Reference,
		TxnDesc:     r.TxnDesc,
	}

	res, err := api.sendSTKPush(tkn, &b)
	return &b, res, err
}

func (api *API) QueryTxnStatus(shortcode string, crid string) (*Query, *Txn, error) {
	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	app, err := api.appStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	tmstmp := nowString()
	pwd := shortcode + app.Passkey() + tmstmp
	pwd = base64.StdEncoding.EncodeToString([]byte(pwd))

	b := Query{
		CRID:      crid,
		Shortcode: shortcode,
		Password:  pwd,
		Timestamp: tmstmp,
	}

	bBytes, err := json.Marshal(&b)
	if err != nil {
		return nil, nil, err
	}

	url := api.env + mpesa.PathSTKPushQuery
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(bBytes))
	if err != nil {
		return nil, nil, err
	}

	req.Header.Set("Authorization", "Bearer "+tkn.Token())
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	res, err := api.client.Post(req)
	if err != nil {
		return nil, nil, err
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		var errResBody mpesa.ErrorResponse
		if err := json.NewDecoder(res.Body).Decode(&errResBody); err != nil {
			return nil, nil, err
		}

		return &b, nil, &errResBody
	}

	var resBody Txn
	if err := json.NewDecoder(res.Body).Decode(&resBody); err != nil {
		return &b, nil, err
	}

	return &b, &resBody, nil
}

func New(env string, clt mpesa.Client, ts mpesa.TokenStore, as mpesa.AppStore, v *validator.Validate) (*API, error) {
	if len(env) == 0 {
		return nil, errors.New("env is blank")
	}

	if clt == nil {
		return nil, errors.New("client is Nil")
	}

	if ts == nil {
		return nil, errors.New("token store is Nil")
	}

	if as == nil {
		return nil, errors.New("app store is Nil")
	}

	if v == nil {
		return nil, errors.New("validator is Nil")
	}

	return &API{env: env, client: clt, tknStore: ts, appStore: as, validate: v}, nil
}
