package express

import (
	"fmt"
	"strconv"
	"time"

	"gitlab.com/petasys/mpesa-go"
)

const (

	// ItemNameAmount is the Amount that was transacted.
	ItemNameAmount = "Amount"

	// ItemNameMpesaReceiptNumber is the unique M-PESA transaction ID for the
	// payment request.
	//
	// The same value is sent to the customer by SMS upon successful processing.
	ItemNameMpesaReceiptNumber = "MpesaReceiptNumber"

	// ItemNameBalance is the Balance of the account for the shortcode used as
	// partyB.
	ItemNameBalance = "Balance"

	// ItemNameTransactionDate is a timestamp that represents the date and time
	// that the transaction was completed in the format YYYYMMDDHHmmss.
	ItemNameTransactionDate = "TransactionDate"

	// ItemNamePhoneNumber is the number of the customer who made the payment.
	ItemNamePhoneNumber = "PhoneNumber"
)

// Item is a key-value data structure for holding weakly linked result parameters
// from M-Pesa.
type Item struct {

	// Name is the item name/key/ID.
	//
	// Optional.
	Name string `json:"Name"`

	// Value is the item contents/value.
	//
	// Optional.
	Value interface{} `json:"Value"`
}

// Metadata is the JSON object that holds more details for the transaction.
type Metadata struct {

	// Items is a JSON Array, within the CallbackMetadata, that holds additional
	// transaction details in JSON objects.
	//
	// Since this array is returned under the CallbackMetadata, it is only returned
	// for successful transactions.
	//
	// Required.
	Items []Item `json:"Item"`
}

type Callback struct {
	// MRID (MerchantRequestID) is a global unique Identifier for any submitted
	// payment request.
	//
	// This is the same value returned in the acknowledgment message of the initial
	// request.
	//
	// Required.
	MRID string `json:"MerchantRequestID" validate:"printascii,max=256"`

	// CRID (CheckoutRequestID) is a globally unique identifier of the processed
	// checkout transaction request.
	//
	// This is the same value returned in the acknowledgment message of the initial request.
	//
	// Required.
	CRID string `json:"CheckoutRequestID" validate:"printascii,max=256"`

	// Code (ResultCode) is a numeric status code that indicates the status of the
	// transaction processing.
	//
	// 0 means successful processing and any other code means an error occurred
	// or the transaction failed.
	//
	// Required.
	Code int `json:"ResultCode"`

	// Desc (ResultDesc) is a message from the API that gives the status
	// of the request processing.
	//
	// It usually maps to a specific ResultCode value.
	//
	// It can be a Success processing message or an error description message.
	//
	// Required.
	Desc string `json:"ResultDesc" validate:"printascii,max=512"`

	// CbMetadata (CallbackMetadata) is the JSON object that holds more details
	// for the transaction.
	//
	// It is only returned for successful transactions.
	//
	// Optional.
	CbMetadata Metadata `json:"CallbackMetadata"`
}

type ResultBody struct {
	STKCallback Callback `json:"stkCallback"`
}

type Result struct {
	Body ResultBody `json:"Body"`
}

// invalidItemValueError returns a general validation error for result parameters
// whose values do not match the expected type.
func invalidItemValueError(name string, v interface{}) error {
	return fmt.Errorf("%v is an invalid %s item value", v, name)
}

func (r *Result) MRID() string {
	return r.Body.STKCallback.MRID
}

func (r *Result) CRID() string {
	return r.Body.STKCallback.CRID
}

func (r *Result) ResultCode() int {
	return r.Body.STKCallback.Code
}

func (r *Result) ResultDesc() string {
	return r.Body.STKCallback.Desc
}

// Successful determines whether the transaction was successful or not.
func (r *Result) Successful() bool {
	return r.Body.STKCallback.Code == 0
}

// resultItemValue extracts and returns a value for a particular key-value
// from the Item key-value list given the key.
func (r *Result) resultItemValue(name string) (interface{}, error) {
	if len(r.Body.STKCallback.CbMetadata.Items) == 0 {
		return nil, fmt.Errorf("Item %s not found. Item list is either Nil or empty", name)
	}

	for _, item := range r.Body.STKCallback.CbMetadata.Items {
		if item.Name == name {
			return item.Value, nil
		}
	}

	return nil, fmt.Errorf("Item %s not found", name)
}

// Amount returns the M-Pesa transaction amount.
func (r *Result) Amount() (float64, error) {
	v, err := r.resultItemValue(ItemNameAmount)
	if err != nil {
		return 0, err
	}

	switch amt := v.(type) {
	case float64:
		return amt, nil
	case int:
		return float64(amt), nil
	default:
		return 0, mpesa.InvalidParameterValueError(ItemNameAmount, v)
	}
}

// Balance returns the Balance of the account for the shortcode used as partyB.
func (r *Result) Balance() (float64, error) {
	v, err := r.resultItemValue(ItemNameBalance)
	if err != nil {
		return 0.0, err
	}

	funds, ok := v.(float64)
	if !ok {
		return 0.0, invalidItemValueError(ItemNameBalance, v)
	}

	return funds, nil
}

// Receipt returns a unique M-PESA transaction ID.
func (r *Result) Receipt() (string, error) {
	v, err := r.resultItemValue(ItemNameMpesaReceiptNumber)
	if err != nil {
		return "", err
	}

	id, ok := v.(string)
	if !ok {
		return "", invalidItemValueError(ItemNameMpesaReceiptNumber, v)
	}

	return id, nil
}

// PhoneNumber returns the number of the customer who made the payment.
func (r *Result) PhoneNumber() (string, error) {
	v, err := r.resultItemValue(ItemNamePhoneNumber)
	if err != nil {
		return "", err
	}

	switch v := v.(type) {
	case string:
		return v, nil
	case int:
		return strconv.Itoa(v), nil
	default:
		return "", invalidItemValueError(ItemNamePhoneNumber, v)
	}
}

// TxnDate returns a timestamp that represents the date and time that the transaction
// was completed.
func (r *Result) TxnDate() (time.Time, error) {
	v, err := r.resultItemValue(ItemNameTransactionDate)
	if err != nil {
		return time.Now(), err
	}

	switch txnDate := v.(type) {
	case string:
		return parseTransactionDate(txnDate)
	case int:
		return parseTransactionDate(strconv.Itoa(txnDate))
	default:
		return time.Now(), invalidItemValueError(ItemNameTransactionDate, v)
	}
}
