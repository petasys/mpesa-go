package mpesa

import (
	"net/http"
	"strings"
)

// Matcher is a function that returns true if a http.Request fulfills a certian
// condition, else, false.
type Matcher func(*http.Request) bool

// Any takes a list of Matchers and returns a Matcher that
// returns true if any Matcher in the list returns true.
func Any(ms ...Matcher) Matcher {
	return func(r *http.Request) bool {
		for _, f := range ms {
			if f(r) {
				return true
			}
		}
		return false
	}
}

// All takes a list of Matchers and returns a Filter that
// returns true only if all Matchers in the list return true.
func All(ms ...Matcher) Matcher {
	return func(r *http.Request) bool {
		for _, f := range ms {
			if !f(r) {
				return false
			}
		}
		return true
	}
}

// Hostname returns a Matcher that returns true if the request's
// hostname matches the provided string.
func Hostname(h string) Matcher {
	return func(r *http.Request) bool {
		return r.URL.Hostname() == h
	}
}

// Path returns a Matcher that returns true if the request's
// path matches the provided string.
func Path(p string) Matcher {
	return func(r *http.Request) bool {
		return r.URL.Path == p
	}
}

// PathPrefix returns a Matcher that returns true if the request's
// path starts with the provided string.
func PathPrefix(p string) Matcher {
	return func(r *http.Request) bool {
		return strings.HasPrefix(r.URL.Path, p)
	}
}

// Method returns a Matcher that returns true if the request
// method is equal to the provided value.
func Method(m string) Matcher {
	return func(r *http.Request) bool {
		return m == r.Method
	}
}
