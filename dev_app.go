package mpesa

import "errors"

// App is a set of OAuth credentials granted by M-Pesa to a developer via the Daraja
// developer portal.
type App interface {
	Passkey() string

	// ConsumerKey returns a OAuth developer app ID/username.
	ConsumerKey() string

	// ConsumerSecret returns a OAuth developer app secret/password.
	ConsumerSecret() string

	// Shortcode returns the unique number that is allocated to a pay bill or buy
	// goods organization through they will be able to receive customer payment.
	//
	// It could be a Pay bill, Buy Goods or Till Number.
	Shortcode() string
}

type AppStore interface {
	Read(shortcode string) (App, error)
	ReadAll() ([]App, error)
	Write(shortcode string, passkey string, consumerKey string, consumerSecret string) error
	Delete(shortcode string) error
}

type defualtApp struct {
	passkey        string
	consumerKey    string
	consumerSecret string
	shortcode      string
}

type InmemoryAppStore struct {
	apps map[string]App
}

func (app *defualtApp) Passkey() string {
	return app.passkey
}

func (app *defualtApp) ConsumerKey() string {
	return app.consumerKey
}

func (app *defualtApp) ConsumerSecret() string {
	return app.consumerSecret
}

func (app *defualtApp) Shortcode() string {
	return app.shortcode
}

func (s *InmemoryAppStore) Read(shortcode string) (App, error) {
	app, ok := s.apps[shortcode]
	if !ok {
		return nil, errors.New("developer App not found")
	}

	return app, nil
}

func (s *InmemoryAppStore) ReadAll() ([]App, error) {
	apps := make([]App, 0, len(s.apps))
	for _, v := range s.apps {
		apps = append(apps, v)
	}

	return apps, nil
}

func (s *InmemoryAppStore) Write(shortcode string, passKey string, consumerKey string, consumerSecret string) error {
	if len(shortcode) == 0 {
		return errors.New("shortcode is blank")
	}

	if len(consumerKey) == 0 {
		return errors.New("consumer key is blank")
	}

	if len(consumerSecret) == 0 {
		return errors.New("consumer secret is blank")
	}

	s.apps[shortcode] = &defualtApp{
		shortcode:      shortcode,
		passkey:        passKey,
		consumerKey:    consumerKey,
		consumerSecret: consumerSecret,
	}
	return nil
}

func (s *InmemoryAppStore) Delete(shortcode string) error {
	delete(s.apps, shortcode)
	return nil
}

func NewInmemoryAppStore() *InmemoryAppStore {
	return &InmemoryAppStore{apps: map[string]App{}}
}
