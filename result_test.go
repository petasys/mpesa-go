package mpesa

import (
	"fmt"
	"testing"
)

func TestNewAmount(t *testing.T) {
	tests := []struct {
		name string
		v    string
		err  string
		amt  *Amount
	}{
		{
			name: "FailToNewAmountIfVDoesntMatch",
			v:    "XYZ",
			err:  "XYZ is an invalid Amount",
		},
		{
			name: "FailToNewAmountIfBasicAmountIsNotNumber",
			v:    "{Amount={CurrencyCode=KES, MinimumAmount=99200, BasicAmount=992.XXX}}",
			err:  "992.XXX is an invalid BasicAmount",
		},
		{
			name: "NewAmount",
			v:    "{Amount={CurrencyCode=KES, MinimumAmount=99200, BasicAmount=992.00}}",
			amt:  &Amount{Currency: "KES", Minimum: "99200", Basic: 992.00},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			amt, err := NewAmount(test.v)
			if err != nil {
				if err.Error() != test.err {
					t.Errorf("NewAmount(%s) = (%v,%v); want = (%v,%v)", test.v, amt, err, test.amt, test.err)
				}
				return
			}

			if amt.Currency != test.amt.Currency {
				t.Errorf("NewAmount(%s) = (%v,%v); want = (%v,%v)", test.v, amt, err, test.amt, test.err)
			}

			if amt.Minimum != test.amt.Minimum {
				t.Errorf("NewAmount(%s) = (%v,%v); want = (%v,%v)", test.v, amt, err, test.amt, test.err)
			}

			if amt.Basic != test.amt.Basic {
				t.Errorf("NewAmount(%s) = (%v,%v); want = (%v,%v)", test.v, amt, err, test.amt, test.err)
			}
		})
	}
}

func TestNewParty(t *testing.T) {
	tests := []struct {
		name string
		v    string
		ok   bool
		c    *Party
	}{
		{
			name: "FailToNewCustomer",
			v:    "",
			ok:   false,
		},
		{
			name: "NewCustomerWith10DigitPhone",
			v:    "0722000000 - Safaricom PLC",
			ok:   true,
			c:    &Party{Name: "Safaricom PLC", Shortcode: "0722000000"},
		},
		{
			name: "NewCustomerWith12DigitPhone",
			v:    "254722000000 - Safaricom PLC",
			ok:   true,
			c:    &Party{Name: "Safaricom PLC", Shortcode: "254722000000"},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			c, ok := NewParty(test.v)
			if ok != test.ok {
				t.Errorf("newCustomer(%s) = (%v,%v); want = (%v,%v)", test.v, c, ok, test.c, test.ok)
			}

			if c == nil {
				return
			}

			if c.Name != test.c.Name {
				t.Errorf("newCustomer(%s) = (%v,%v); want = (%v,%v)", test.v, c, ok, test.c, test.ok)
			}

			if c.Shortcode != test.c.Shortcode {
				t.Errorf("newCustomer(%s) = (%v,%v); want = (%v,%v)", test.v, c, ok, test.c, test.ok)
			}
		})
	}
}

func TestCustomerString(t *testing.T) {
	c := &Party{Name: "Safaricom PLC", Shortcode: "254722000000"}
	want := "254722000000 - Safaricom PLC"
	if got := c.String(); got != want {
		t.Errorf("String() = %s; want = %s", got, want)
	}
}

func TestInvalidParameterValueError(t *testing.T) {
	want := fmt.Errorf("%v is an invalid %s result parameter value", 0, "XYZ")
	got := InvalidParameterValueError("XYZ", 0)
	if want.Error() != got.Error() {
		t.Errorf("InvalidParameterValueError(%s,%v) = %s; want = %s", "XYZ", 0, got, want)
	}
}
