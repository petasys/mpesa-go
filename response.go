package mpesa

import (
	"encoding/json"
	"fmt"
)

const (
	ResponseCodeSuccessful = "0"
)

// Response is a synchronous notification sent out as soon as the request has been
// authorized, authenticated and accepted for processing.
type Response struct {

	// CID is a global unique identifier for the transaction request returned
	// by the API proxy upon successful request submission.
	//
	// Required.
	CID string `json:"ConversationID" validate:"required,printascii,max=256"`

	// OCID is a global unique identifier for the transaction request returned
	// by the M-PESA upon successful request submission.
	//
	// OCID can be found on the M-Pesa statement downloaded from the M-Pesa
	// portal and is crucial during any manual reconciliation. Please save the
	// OCID to a file or database table in case it is required in the future.
	//
	// Required.
	OCID string `json:"OriginatorConversationID" validate:"required,printascii,max=256"`

	// Code is a transaction status code returned by M-PESA upon successful request
	// submission.
	//
	// 0 means success. Anything else is fail.
	//
	// Required.
	Code string `json:"ResponseCode" validate:"numeric"`

	// Desc is the description of the request submission status.
	//
	// Required.
	Desc string `json:"ResponseDescription" validate:"required,max=2048"`
}

// ErrorResponse is a synchronous notification sent out as soon as the request
// has failed to go through.
//
// ErrorResponse implements error.
type ErrorResponse struct {

	// ID is a unique requestID for the payment request.
	//
	// Required.
	ID string `json:"requestId"`

	// Code is a predefined code that indicates the reason for a request failure.
	//
	// Required.
	Code string `json:"errorCode"`

	// Message is a short descriptive message of the failure reason.
	//
	// Required.
	Message string `json:"errorMessage"`
}

// Successful returns true if the synchronous response from M-Pesa was successful,
// false otherwise.
func (r *Response) Successful() bool {
	return r.Code == ResponseCodeSuccessful
}

// String returns a text format of Response by marshalling it into a JSON object.
func (r *Response) String() string {
	v, err := json.Marshal(r)
	if err != nil {
		return "{}"
	}

	return string(v)
}

// Error returns ErrorResponse in text format.
func (r *ErrorResponse) Error() string {
	return fmt.Sprintf("{requestId: %s,errorCode: %s, errorMessage: %s}", r.ID, r.Code, r.Message)
}

// String returns a text format of ErrorResponse by marshalling it into a JSON object.
func (r *ErrorResponse) String() string {
	v, err := json.Marshal(r)
	if err != nil {
		return "{}"
	}

	return string(v)
}
