package oauth

import (
	"errors"
	"log"
	"sync"
	"time"

	"gitlab.com/petasys/mpesa-go"
)

var listener ExpiryListener
var lock = &sync.Mutex{}

type ExpiryListener interface {
	Listen()
}

type defaultExpiryListener struct {
	api      *API
	appStore mpesa.AppStore
	tknStore mpesa.TokenStore

	ticker *time.Ticker
}

func (lstr *defaultExpiryListener) Listen() {
	go lstr.init()
	go lstr.listenAndRefresh()
}

func (lstr *defaultExpiryListener) init() {
	apps, err := lstr.appStore.ReadAll()
	if err != nil {
		log.Println("TokenExpiryListener.init():", err)
		return
	}

	if len(apps) == 0 {
		return
	}

	pendingApps := map[string]mpesa.App{}
	for _, app := range apps {
		pendingApps[app.Shortcode()] = app
	}

	ticker := time.NewTicker(time.Minute * 5)

	for {
		for k, v := range pendingApps {
			go func(shortcode string, app mpesa.App) {
				tkn, exp, err := lstr.api.Generate(app.ConsumerKey(), app.ConsumerSecret())
				if err != nil {
					log.Println("TokenExpiryListener.init():", err)
					return
				}

				if err := lstr.tknStore.Write(shortcode, tkn, exp); err != nil {
					log.Println("TokenExpiryListener.init():", err)
					return
				}

				delete(pendingApps, shortcode)
			}(k, v)
		}

		// Wait for 5 mins
		<-ticker.C

		if len(pendingApps) == 0 {
			break
		}
	}

}

func (lstr *defaultExpiryListener) listenAndRefresh() {
	for range lstr.ticker.C {
		tkns, err := lstr.tknStore.ReadExpired()
		if err != nil {
			log.Println("TokenExpiryListener.listenAndRefresh():", err)
			return
		}

		for _, tkn := range tkns {
			go func(old mpesa.Token) {
				if !old.Expired() {
					return
				}

				app, err := lstr.appStore.Read(old.Shortcode())
				if err != nil {
					log.Println("TokenExpiryListener.refreshToken():", err)
					return
				}

				if app.Shortcode() != old.Shortcode() {
					log.Println("TokenExpiryListener.refreshToken(): AppStore and TokenStore shortcode mismatch")
					return
				}

				tkn, exp, err := lstr.api.Generate(app.ConsumerKey(), app.ConsumerSecret())
				if err != nil {
					log.Println("TokenExpiryListener.refreshToken():", err)
					return
				}

				if err := lstr.tknStore.Write(app.Shortcode(), tkn, exp); err != nil {
					log.Println("TokenExpiryListener.refreshToken():", err)
				}
			}(tkn)
		}
	}
}

func ExpiryListenerInstance(api *API, as mpesa.AppStore, ts mpesa.TokenStore) (ExpiryListener, error) {
	if listener == nil {
		lock.Lock()
		defer lock.Unlock()

		if api == nil {
			return nil, errors.New("api is Nil")
		}

		if as == nil {
			return nil, errors.New("app store is Nil")
		}

		if ts == nil {
			return nil, errors.New("token store is Nil")
		}

		ticker := time.NewTicker(time.Minute)
		listener = &defaultExpiryListener{api: api, appStore: as, tknStore: ts, ticker: ticker}
	}

	return listener, nil
}
