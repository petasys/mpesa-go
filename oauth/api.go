// Copyright 2023 M-Pesa Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// This API generates the tokens for authenticating your API calls. This is the
// first API you will engage with within the set of APIs available because all
// the other APIs require authentication information from this API to work.
package oauth

import (
	"encoding/json"
	"errors"
	"net/http"
	"strconv"
	"strings"
	"time"

	"gitlab.com/petasys/mpesa-go"
)

type API struct {
	client mpesa.Client
	env    string
}

func (api *API) Generate(key string, secret string) (string, time.Duration, error) {
	url := api.env + mpesa.PathOAuth

	req, err := http.NewRequest(http.MethodGet, url, nil)
	if err != nil {
		return "", time.Duration(0), err
	}

	req.SetBasicAuth(key, secret)

	res, err := api.client.Get(req)
	if err != nil {
		return "", time.Duration(0), err
	}

	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		return "", time.Duration(0), errors.New(res.Status)
	}

	var b map[string]string
	if err := json.NewDecoder(res.Body).Decode(&b); err != nil {
		return "", time.Duration(0), err
	}

	token, ok := b["access_token"]
	if !ok {
		return "", time.Duration(0), errors.New("HTTP error: response is malformed")
	}

	exp := time.Second * 3599
	v, ok := b["expires_in"]
	if ok {
		if newExp, err := strconv.ParseInt(v, 10, 32); err == nil {
			exp = time.Second * time.Duration(newExp)
		}
	}

	return token, exp, nil
}

func New(env string, client mpesa.Client) (*API, error) {
	if len(env) == 0 {
		return nil, errors.New("env is empty. The base URL cannot be Nil or Empty")
	}

	if client == nil {
		return nil, errors.New("client is nil. The M-Pesa API client must be initialized")
	}

	return &API{env: strings.Trim(env, "/"), client: client}, nil
}
