package b2c

// Request is a definition of the HTTP request body fomart for the Safaricom's
// Daraja B2C RESTful API.
type Request struct {

	// Initiator is an API user created by the Business Administrator of the
	// M-PESA Bulk disbursement account that is active and authorized to
	// initiate B2C transactions via API.
	//
	// Required.
	Initiator string `json:"InitiatorName" validate:"required,max=256"`

	// Credential is the value obtained after encrypting the API initiator
	// password.
	//
	// The password on Sandbox has been provisioned on the simulator. However,
	// on production the password is created when the user is being created on
	// the M-PESA organization portal.
	//
	// Required.
	Credential string `json:"SecurityCredential" validate:"base64"`

	// CommandID is a unique command that specifies B2C transaction type.
	//
	// SalaryPayment: This supports sending money to both registered and unregistered
	// M-Pesa customers.
	//
	// BusinessPayment: This is a normal business to customer payment, supports
	// only M-PESA registered customers.
	//
	// PromotionPayment: This is a promotional payment to customers. The M-PESA
	// notification message is a congratulatory message. Supports only M-PESA registered
	// customers.
	//
	// Required.
	CommandID string `json:"CommandID" validate:"oneof=SalaryPayment BusinessPayment PromotionPayment"`

	// Amount amount of money being sent to the customer.
	//
	// Required.
	Amount float64 `json:"Amount" validate:"min=1,max=150000"`

	// PartyA is the B2C organization shortcode from which the money is sent from.
	// A shortcode is 5 - 6 digits long e.g. 123454.
	//
	// Required.
	PartyA string `json:"PartyA" validate:"numeric,min=5,max=16"`

	// PartyB is the customer mobile number to receive the amount. The number
	// should have the country code (254) without the plus sign e.g. 254722000000
	//
	// Required.
	PartyB string `json:"PartyB" validate:"startswith=254,len=12,numeric"`

	// Remarks is any additional information to be associated with the transaction. Remarks
	// can only be 100 characters long or less.
	//
	// Required.
	Remarks string `json:"Remarks" validate:"max=100"`

	// QueueTimeoutURL  is the URL to be specified in your request that will be used
	// by API Proxy to send notification incase the payment request is timed out while
	// awaiting processing in the queue.
	//
	// Required.
	QueueTimeoutURL string `json:"QueueTimeOutURL" validate:"http_url"`

	// ResultURL is the URL to be specified in your request that will be used by
	// M-PESA to send notification upon processing of the payment request.
	//
	// Required.
	ResultURL string `json:"ResultURL" validate:"http_url"`

	// Occassion, just like Remarks, is any additional information to be associated
	// with the transaction. Occassion is optional and can only be 100 characters
	// long or less.
	//
	// Optional.
	Occassion string `json:"Occassion,omitempty" validate:"max=100"`
}

// SendMoneyInfo is a data transfer object used by developers to supply the framework
// with the required B2C request data.
//
// SendMoneyInfo if not sent to M-Pesa. It is converted to fullRequest by adding auth
// info and transaction type info depending on the function called.
type SendMoneyInfo struct {

	// PartyB is the customer mobile number to receive the amount. The number
	// should have the country code (254) without the plus sign e.g. 254722000000
	//
	// Required.
	PartyB string `validate:"startswith=254,len=12,numeric"`

	// Amount amount of money being sent to the customer.
	//
	// Required.
	Amount float64 `validate:"min=1,max=150000"`

	// Remarks is any additional information to be associated with the transaction. Remarks
	// can only be 100 characters long or less.
	//
	// Required.
	Remarks string `validate:"min=2,max=100"`

	// Occassion, just like Remarks, is any additional information to be associated
	// with the transaction. Occassion is optional and can only be 100 characters
	// long or less.
	//
	// Optional.
	Occassion string `validate:"max=100"`

	// QueueTimeoutURL  is the URL to be specified in your request that will be used
	// by API Proxy to send notification incase the payment request is timed out while
	// awaiting processing in the queue.
	//
	// Required.
	QueueTimeoutURL string `validate:"http_url"`

	// ResultURL is the URL to be specified in your request that will be used by
	// M-PESA to send notification upon processing of the payment request.
	//
	// Required.
	ResultURL string `validate:"http_url"`
}
