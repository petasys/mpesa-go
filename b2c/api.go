// Copyright 2023 M-Pesa Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// B2C API is an API used to make payments from a Business to Customers (Pay Outs),
// also known as Bulk Disbursements.
//
// B2C API is used in several scenarios by businesses that require to either make
// Salary Payments, Cashback payments, Promotional Payments(e.g. betting winning payouts),
// winnings, financial institutions withdrawal of funds, loan disbursements, etc.
package b2c

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"gitlab.com/petasys/mpesa-go"
)

// API is a implementation of the 3 B2C APIs provided by M-Pesa via Daraja.
type API struct {

	// env is the B2C API host name.
	env string

	// client is the underlying HTTP client with support for interceptors e.t.c.
	client mpesa.Client

	// tknStore is a store where the access token is retrived for each transaction.
	tknStore mpesa.TokenStore

	// initStore is a store where the M-Pesa initiator name and password are stored.
	initStore mpesa.InitiatorStore

	// encryptor is an API for encrypting initiator password using RSA pub key and
	// encoding the result using base64.
	encryptor mpesa.SCEncryptor

	// validate is used to run validation checks for all request params. The idea is to
	// cancel an invalid request before it makes it to M-Pesa.
	validate *validator.Validate
}

// sendMoney sends money from a B2C organisation shortcode to a registered/unregistered
// customer.
//
// All B2C API functions delegate to this generic API impelementation.
func (api *API) sendMoney(tkn mpesa.Token, b *Request) (*mpesa.Response, error) {
	bBytes, err := json.Marshal(&b)
	if err != nil {
		return nil, mpesa.NewAPIError(true, err, "b2c.Request to JSON conversion failed")
	}

	url := api.env + mpesa.PathB2C
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(bBytes))
	if err != nil {
		return nil, mpesa.NewAPIError(true, err, "Creating a new http.Request failed")
	}

	req.Header.Set("Authorization", "Bearer "+tkn.Token())
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	res, err := api.client.Post(req)
	if err != nil {
		return nil, mpesa.NewAPIError(false, err, "Network error occurred")
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		var errBody mpesa.ErrorResponse
		if err := json.NewDecoder(res.Body).Decode(&errBody); err != nil {
			return nil, mpesa.NewAPIError(false, err, "JSON to mpesa.ErrorResponse conversion failed")
		}

		return nil, &errBody
	}

	var successBody mpesa.Response
	if err := json.NewDecoder(res.Body).Decode(&successBody); err != nil {
		return nil, mpesa.NewAPIError(false, err, "JSON to mpesa.Response conversion failed")
	}

	return &successBody, nil
}

// SendSalary sends money to both registered and unregistered M-Pesa customers using
// SalaryPayment B2C API command ID.
func (api *API) SendSalary(shortcode string, r *SendMoneyInfo) (*Request, *mpesa.Response, error) {
	if err := api.validate.Struct(r); err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "b2c.SendMoneyInfo is invalid")
	}

	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Token not found for shortcode "+shortcode)
	}

	init, err := api.initStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator not found for shortcode "+shortcode)
	}

	credential, err := api.encryptor.Encrypt(init.Password())
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator password encryption failed")
	}

	b := Request{
		CommandID:       mpesa.CommandIDSalaryPayment,
		Initiator:       init.Name(),
		Credential:      credential,
		PartyA:          shortcode,
		PartyB:          r.PartyB,
		Amount:          r.Amount,
		Remarks:         r.Remarks,
		Occassion:       r.Occassion,
		ResultURL:       r.ResultURL,
		QueueTimeoutURL: r.QueueTimeoutURL,
	}

	res, err := api.sendMoney(tkn, &b)
	return &b, res, err
}

// SendPromotion sends promotional payment to customers using the PromotionPayment
// B2C API command ID.
//
// The M-PESA notification message is a congratulatory message. Supports only M-PESA
// registered customers.
func (api *API) SendPromotion(shortcode string, r *SendMoneyInfo) (*Request, *mpesa.Response, error) {
	if err := api.validate.Struct(r); err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "b2c.SendMoneyInfo is invalid")
	}

	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Token not found for shortcode "+shortcode)
	}

	init, err := api.initStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator not found for shortcode "+shortcode)
	}

	credential, err := api.encryptor.Encrypt(init.Password())
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator password encryption failed")
	}

	b := Request{
		CommandID:       mpesa.CommandIDPromotionPayment,
		Initiator:       init.Name(),
		Credential:      credential,
		PartyA:          shortcode,
		PartyB:          r.PartyB,
		Amount:          r.Amount,
		Remarks:         r.Remarks,
		Occassion:       r.Occassion,
		ResultURL:       r.ResultURL,
		QueueTimeoutURL: r.QueueTimeoutURL,
	}

	res, err := api.sendMoney(tkn, &b)
	return &b, res, err
}

// SendMoney sends money from a B2C organization shortcode to a registered
// M-Pesa customers only.
//
// This is a normal business to customer payment.
func (api *API) SendBusinessPayment(shortcode string, r *SendMoneyInfo) (*Request, *mpesa.Response, error) {
	if err := api.validate.Struct(r); err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "b2c.SendMoneyInfo is invalid")
	}

	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Token not found for shortcode "+shortcode)
	}

	init, err := api.initStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator not found for shortcode "+shortcode)
	}

	credential, err := api.encryptor.Encrypt(init.Password())
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator password encryption failed")
	}

	b := Request{
		CommandID:       mpesa.CommandIDBusinessPayment,
		Initiator:       init.Name(),
		Credential:      credential,
		PartyA:          shortcode,
		PartyB:          r.PartyB,
		Amount:          r.Amount,
		Remarks:         r.Remarks,
		Occassion:       r.Occassion,
		ResultURL:       r.ResultURL,
		QueueTimeoutURL: r.QueueTimeoutURL,
	}

	res, err := api.sendMoney(tkn, &b)
	return &b, res, err
}

// New creates a new instance of API.
func New(env string, clt mpesa.Client, ts mpesa.TokenStore, is mpesa.InitiatorStore, v *validator.Validate,
	enc mpesa.SCEncryptor) (*API, error) {

	if len(env) == 0 {
		return nil, errors.New("env is blank")
	}

	if clt == nil {
		return nil, errors.New("client is Nil")
	}

	if ts == nil {
		return nil, errors.New("token store is Nil")
	}

	if is == nil {
		return nil, errors.New("initiator store is Nil")
	}

	if v == nil {
		return nil, errors.New("validator is Nil")
	}

	if enc == nil {
		return nil, errors.New("security credentials encryptor is Nil")
	}

	return &API{env: strings.Trim(env, "/"), client: clt, tknStore: ts, initStore: is, validate: v, encryptor: enc}, nil
}
