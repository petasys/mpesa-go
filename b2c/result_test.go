package b2c

import (
	"fmt"
	"testing"

	"github.com/go-playground/validator/v10"
	"gitlab.com/petasys/mpesa-go"
)

func TestResultCID(t *testing.T) {
	r := Result{Result: ResultData{CID: "CID"}}
	if got := r.CID(); got != "CID" {
		t.Errorf("CID() = %s; want = CID", got)
	}
}

func TestResultOCID(t *testing.T) {
	r := Result{Result: ResultData{OCID: "OCID"}}
	if got := r.OCID(); got != "OCID" {
		t.Errorf("OCID() = %s; want = OCID", got)
	}
}

func TestResultType(t *testing.T) {
	r := Result{Result: ResultData{Type: 0}}
	if got := r.ResultType(); got != 0 {
		t.Errorf("ResultType() = %d; want = 0", got)
	}
}

func TestResultCode(t *testing.T) {
	r := Result{Result: ResultData{Code: 0}}
	if got := r.ResultCode(); got != 0 {
		t.Errorf("ResultCode() = %d; want = 0", got)
	}
}

func TestResultDesc(t *testing.T) {
	r := Result{Result: ResultData{Desc: "ResultDesc"}}
	if got := r.ResultDesc(); got != "ResultDesc" {
		t.Errorf("ResultDesc() = %s; want = ResultDesc", got)
	}
}

func TestResultSuccessful(t *testing.T) {
	tests := []struct {
		name string
		code int
		want bool
	}{
		{name: "FailedIfCodeIsNotZero", code: 2001, want: false},
		{name: "SuccessfulIfCodeIsZero", code: 0, want: true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{Code: test.code}}
			if got := r.Successful(); got != test.want {
				t.Errorf("Successful() = %v; want = %v", got, test.want)
			}
		})
	}
}

func TestResultAmount(t *testing.T) {
	tests := []struct {
		name   string
		params []mpesa.Kv
		v      interface{}
		err    string
	}{
		{
			name:   "FailToAmountIfResultParameterValueFails",
			params: []mpesa.Kv{},
			err:    "ResultParameter TransactionAmount not found. ResultParameter list is either Nil or empty",
		},
		{
			name:   "FailToAmountIfParameterTransactionAmountIsNotInteger",
			params: []mpesa.Kv{{K: ParameterKeyTransactionAmount, V: "100.00"}},
			err:    mpesa.InvalidParameterValueError(ParameterKeyTransactionAmount, "100.00").Error(),
		},
		{
			name:   "Amount",
			params: []mpesa.Kv{{K: ParameterKeyTransactionAmount, V: 100}},
			v:      100,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{Parameters: mpesa.Parameters{Parameter: test.params}}}
			v, err := r.Amount()
			if err != nil {
				if err.Error() != test.err {
					t.Errorf("Amount() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
				}
				return
			}

			if v != test.v {
				t.Errorf("Amount() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
			}
		})
	}
}

func TestResultReceipt(t *testing.T) {
	tests := []struct {
		name   string
		id     string
		params []mpesa.Kv
		v      interface{}
		err    string
	}{
		{
			name:   "FailToReceiptIfResultParameterValueFails",
			params: []mpesa.Kv{},
			err:    "ResultParameter TransactionReceipt not found. ResultParameter list is either Nil or empty",
		},
		{
			name:   "FailToReceiptIfParameterTransactionReceiptIsNotString",
			params: []mpesa.Kv{{K: ParameterKeyTransactionReceipt, V: 100}},
			err:    mpesa.InvalidParameterValueError(ParameterKeyTransactionReceipt, 100).Error(),
		},
		{
			name:   "ReceiptAsParameterKeyTransactionReceipt",
			params: []mpesa.Kv{{K: ParameterKeyTransactionReceipt, V: "Receipt"}},
			v:      "Receipt",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{ID: test.id, Parameters: mpesa.Parameters{Parameter: test.params}}}
			v, err := r.TransactionReceipt()
			if err != nil {
				if err.Error() != test.err {
					t.Errorf("TransactionReceipt() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
				}
				return
			}

			if v != test.v {
				t.Errorf("TransactionReceipt() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
			}
		})
	}
}

func TestResultWorkingAccountAvailableFunds(t *testing.T) {
	tests := []struct {
		name   string
		params []mpesa.Kv
		v      interface{}
		err    string
	}{
		{
			name:   "FailToWorkingAccountAvailableFundsIfResultParameterValueFails",
			params: []mpesa.Kv{},
			err:    "ResultParameter B2CWorkingAccountAvailableFunds not found. ResultParameter list is either Nil or empty",
		},
		{
			name:   "FailToWorkingAccountAvailableFundsIfVIsNotFloat",
			params: []mpesa.Kv{{K: ParameterKeyB2CWorkingAccountAvailableFunds, V: "100.00"}},
			err:    mpesa.InvalidParameterValueError(ParameterKeyB2CWorkingAccountAvailableFunds, "100.00").Error(),
		},
		{
			name:   "WorkingAccountAvailableFunds",
			params: []mpesa.Kv{{K: ParameterKeyB2CWorkingAccountAvailableFunds, V: 100.0}},
			v:      100.0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{Parameters: mpesa.Parameters{Parameter: test.params}}}
			v, err := r.B2CWorkingAccountAvailableFunds()
			if err != nil {
				if err.Error() != test.err {
					t.Errorf("B2CWorkingAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
				}
				return
			}

			if v != test.v {
				t.Errorf("B2CWorkingAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
			}
		})
	}
}

func TestResultUtilityAccountAvailableFunds(t *testing.T) {
	tests := []struct {
		name   string
		params []mpesa.Kv
		v      interface{}
		err    string
	}{
		{
			name:   "FailToUtilityAccountAvailableFundsIfResultParameterValueFails",
			params: []mpesa.Kv{},
			err:    "ResultParameter B2CUtilityAccountAvailableFunds not found. ResultParameter list is either Nil or empty",
		},
		{
			name:   "FailToUtilityAccountAvailableFundsIfVIsNotFloat",
			params: []mpesa.Kv{{K: ParameterKeyB2CUtilityAccountAvailableFunds, V: "100.00"}},
			err:    mpesa.InvalidParameterValueError(ParameterKeyB2CUtilityAccountAvailableFunds, "100.00").Error(),
		},
		{
			name:   "UtilityAccountAvailableFunds",
			params: []mpesa.Kv{{K: ParameterKeyB2CUtilityAccountAvailableFunds, V: 100.0}},
			v:      100.0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{Parameters: mpesa.Parameters{Parameter: test.params}}}
			v, err := r.B2CUtilityAccountAvailableFunds()
			if err != nil {
				if err.Error() != test.err {
					t.Errorf("B2CUtilityAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
				}
				return
			}

			if v != test.v {
				t.Errorf("B2CUtilityAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
			}
		})
	}
}

func TestResultChargesPaidAccountAvailableFunds(t *testing.T) {
	tests := []struct {
		name   string
		params []mpesa.Kv
		v      interface{}
		err    string
	}{
		{
			name:   "FailToChargesPaidAccountAvailableFundsIfResultParameterValueFails",
			params: []mpesa.Kv{},
			err:    "ResultParameter B2CChargesPaidAccountAvailableFunds not found. ResultParameter list is either Nil or empty",
		},
		{
			name:   "FailToChargesPaidAccountAvailableFundsIfVIsNotFloat",
			params: []mpesa.Kv{{K: ParameterKeyB2CChargesPaidAccountAvailableFunds, V: "100.00"}},
			err:    mpesa.InvalidParameterValueError(ParameterKeyB2CChargesPaidAccountAvailableFunds, "100.00").Error(),
		},
		{
			name:   "ChargesPaidAccountAvailableFunds",
			params: []mpesa.Kv{{K: ParameterKeyB2CChargesPaidAccountAvailableFunds, V: 100.0}},
			v:      100.0,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{Parameters: mpesa.Parameters{Parameter: test.params}}}
			v, err := r.ChargesPaidAccountAvailableFunds()
			if err != nil {
				if err.Error() != test.err {
					t.Errorf("ChargesPaidAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
				}
				return
			}

			if v != test.v {
				t.Errorf("ChargesPaidAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
			}
		})
	}
}

func TestResultCompletedDateTime(t *testing.T) {
	tests := []struct {
		name   string
		params []mpesa.Kv
		v      interface{}
		err    string
	}{
		{
			name:   "FailToCompletedDateTimeIfResultParameterValueFails",
			params: []mpesa.Kv{},
			err:    "ResultParameter TransactionCompletedDateTime not found. ResultParameter list is either Nil or empty",
		},
		{
			name:   "FailToCompletedDateTimeIfCompletedDateTimeIsNotString",
			params: []mpesa.Kv{{K: ParameterKeyTransactionCompletedDateTime, V: 100}},
			err:    mpesa.InvalidParameterValueError(ParameterKeyTransactionCompletedDateTime, 100).Error(),
		},
		{
			name:   "CompletedDateTime",
			params: []mpesa.Kv{{K: ParameterKeyTransactionCompletedDateTime, V: "CompletedDateTime"}},
			v:      "CompletedDateTime",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{Parameters: mpesa.Parameters{Parameter: test.params}}}
			v, err := r.CompletedDateTime()
			if err != nil {
				if err.Error() != test.err {
					t.Errorf("CompletedDateTime() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
				}
				return
			}

			if v != test.v {
				t.Errorf("CompletedDateTime() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
			}
		})
	}
}

func TestResultReceiverPartyPublicName(t *testing.T) {
	tests := []struct {
		name   string
		params []mpesa.Kv
		v      interface{}
		err    string
		c      *mpesa.Party
	}{
		{
			name:   "FailToReceiverPartyPublicNameIfResultParameterValueFails",
			params: []mpesa.Kv{},
			err:    "ResultParameter ReceiverPartyPublicName not found. ResultParameter list is either Nil or empty",
		},
		{
			name:   "FailToReceiverPartyPublicNameIfVIsNotString",
			params: []mpesa.Kv{{K: ParameterKeyReceiverPartyPublicName, V: 100}},
			err:    mpesa.InvalidParameterValueError(ParameterKeyReceiverPartyPublicName, 100).Error(),
		},
		{
			name:   "ReceiverPartyPublicNameWithoutCustomer",
			params: []mpesa.Kv{{K: ParameterKeyReceiverPartyPublicName, V: "0070000000Test Test"}},
			v:      "0070000000Test Test",
		},
		{
			name:   "ReceiverPartyPublicNameWithCustomer",
			params: []mpesa.Kv{{K: ParameterKeyReceiverPartyPublicName, V: "0070000000 - Test Test"}},
			v:      "0070000000 - Test Test",
			c:      &mpesa.Party{Name: "Test Test", Shortcode: "0070000000"},
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{Parameters: mpesa.Parameters{Parameter: test.params}}}
			c, v, err := r.ReceiverPartyPublicName()
			if err != nil {
				if err.Error() != test.err {
					t.Errorf("ReceiverPartyPublicName() = (%v,%v,%v); want = (%v,%s)", c, v, err, test.v, test.err)
				}
				return
			}

			if v != test.v {
				t.Errorf("ReceiverPartyPublicName() = (%v,%v,%v); want = (%v,%s)", c, v, err, test.v, test.err)
			}

			if c == nil {
				return
			}

			if c.Name != test.c.Name {
				t.Errorf("ReceiverPartyPublicName() = (%v,%v,%v); want = (%v,%s)", c, v, err, test.v, test.err)
			}

			if c.Shortcode != test.c.Shortcode {
				t.Errorf("ReceiverPartyPublicName() = (%v,%v,%v); want = (%v,%s)", c, v, err, test.v, test.err)
			}

		})
	}
}

func TestResultRecipientIsRegisteredCustomer(t *testing.T) {
	tests := []struct {
		name   string
		params []mpesa.Kv
		v      interface{}
		err    string
	}{
		{
			name:   "FailToRecipientIsRegisteredCustomerIfResultParameterValueFails",
			params: []mpesa.Kv{},
			err:    "ResultParameter B2CRecipientIsRegisteredCustomer not found. ResultParameter list is either Nil or empty",
		},
		{
			name:   "FailToRecipientIsRegisteredCustomerIfVIsNotString",
			params: []mpesa.Kv{{K: ParameterKeyB2CRecipientIsRegisteredCustomer, V: 100}},
			err:    mpesa.InvalidParameterValueError(ParameterKeyB2CRecipientIsRegisteredCustomer, 100).Error(),
		},
		{
			name:   "RecipientIsRegisteredCustomer",
			params: []mpesa.Kv{{K: ParameterKeyB2CRecipientIsRegisteredCustomer, V: "Y"}},
			v:      true,
		},
		{
			name:   "RecipientIsUnregisteredCustomer",
			params: []mpesa.Kv{{K: ParameterKeyB2CRecipientIsRegisteredCustomer, V: "N"}},
			v:      false,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{Parameters: mpesa.Parameters{Parameter: test.params}}}
			v, err := r.RecipientIsRegisteredCustomer()
			if err != nil {
				if err.Error() != test.err {
					t.Errorf("RecipientIsRegisteredCustomer() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
				}
				return
			}

			if v != test.v {
				t.Errorf("RecipientIsRegisteredCustomer() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
			}
		})
	}
}

func TestResultString(t *testing.T) {
	tests := []struct {
		name   string
		params []mpesa.Kv
	}{
		{name: "StringIfJSONMarshalFails", params: []mpesa.Kv{{K: "", V: make(chan int)}}},
		{name: "String", params: []mpesa.Kv{}},
	}

	v := validator.New()
	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Result{Result: ResultData{Parameters: mpesa.Parameters{Parameter: test.params}}}
			text := r.String()
			if err := v.Var(text, "json"); err != nil {
				t.Errorf("String() = %s; want = JSON object", text)
			}
		})
	}
}

func TestInvalidParameterValueError(t *testing.T) {
	want := fmt.Errorf("%v is an invalid %s result parameter value", 0, ParameterKeyTransactionAmount)
	got := mpesa.InvalidParameterValueError(ParameterKeyTransactionAmount, 0)
	if want.Error() != got.Error() {
		t.Errorf("mpesa.InvalidParameterValueError(%s,%v) = %s; want = %s", ParameterKeyTransactionAmount, 0, got, want)
	}
}
