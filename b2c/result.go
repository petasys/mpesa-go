package b2c

import (
	"encoding/json"
	"strings"

	"gitlab.com/petasys/mpesa-go"
)

const (

	// ParameterKeyTransactionAmount is the amount that is transacted e.g. 100.
	//
	// It is also returned under the ResultParameter array.
	ParameterKeyTransactionAmount = "TransactionAmount"

	// ParameterKeyTransactionReceipt is a unique M-PESA transaction ID for every
	// payment request e.g. LHG31AA5TX.
	//
	// The same value is sent to a customer over SMS upon successful processing.
	//
	// It is usually returned under the ResultParameter array.
	ParameterKeyTransactionReceipt = "TransactionReceipt"

	// ParameterKeyB2CWorkingAccountAvailableFunds is the available balance of the
	// Working account under the B2C shortcode used in the transaction e.g. 2000.0.
	ParameterKeyB2CWorkingAccountAvailableFunds = "B2CWorkingAccountAvailableFunds"

	// ParameterKeyB2CUtilityAccountAvailableFunds is the available balance of the
	// Utility account under the B2C shortcode used in the transaction e.g. 23654.5.
	ParameterKeyB2CUtilityAccountAvailableFunds = "B2CUtilityAccountAvailableFunds"

	// ParameterKeyTransactionCompletedDateTime is the date and time that the transaction
	// completed M-PESA e.g. 01.08.2018 16:12:12.
	ParameterKeyTransactionCompletedDateTime = "TransactionCompletedDateTime"

	// ParameterKeyReceiverPartyPublicName is the name and phone number of the customer
	// who received the payment e.g. 254722000000 - Safaricom PLC.
	ParameterKeyReceiverPartyPublicName = "ReceiverPartyPublicName"

	// ParameterKeyB2CChargesPaidAccountAvailableFunds is the available balance of
	// the Charges Paid account under the B2C shortcode used in the transaction.
	ParameterKeyB2CChargesPaidAccountAvailableFunds = "B2CChargesPaidAccountAvailableFunds"

	// ParameterKeyB2CRecipientIsRegisteredCustomer is a key that indicates whether
	// the customer is a M-PESA registered customer or not.
	//
	// "Y" for Yes and "N" for No.
	ParameterKeyB2CRecipientIsRegisteredCustomer = "B2CRecipientIsRegisteredCustomer"
)

// Reference is JSON object that holds more details about the result.
type ReferenceData struct {
	Item mpesa.Kv `json:"ReferenceItem"`
}

// ResultData is the root parameter that encloses the entire result message.
type ResultData struct {

	// ID is a unique M-PESA transaction ID for every payment request. Same
	// value is sent to customer over SMS upon successful processing e.g. LHG31AA5TX.
	//
	// Required.
	ID string `json:"TransactionID" validate:"len=10,alphanum,uppercase"`

	// CID is a global unique identifier for the transaction request returned
	// by the M-PESA upon successful request submission e.g. 236543-276372-2.
	//
	// Required.
	CID string `json:"ConversationID" validate:"required,printascii,max=256"`

	// OCID is a global unique identifier for the transaction request returned
	// by the API proxy upon successful request submission e.g. AG_2376487236_126732989KJHJKH.
	//
	// Required.
	OCID string `json:"OriginatorConversationID" validate:"required,printascii,max=256"`

	// Type is a status code that indicates whether the transaction was already
	// sent to your listener. Usual value is 0.
	//
	// Required.
	Type int `json:"ResultType" validate:"min=0"`

	// Code is a numeric status code that indicates the status of the transaction
	// processing.
	//
	// 0 means success and any other code means an error occurred or the transaction
	// failed. Possible values are 0 and 2001.
	//
	// Required.
	Code int `json:"ResultCode" validate:"min=0"`

	// Desc is a message from the API that gives the status of the request
	// processing and usually maps to a specific result code value.
	//
	// Required.
	Desc string `json:"ResultDesc" validate:"required,max=2048"`

	// Reference is JSON object that holds more details about the result.
	//
	// Optional.
	Reference ReferenceData `json:"ReferenceData"`

	// Parameters is a JSON object that holds more details for the transaction.
	//
	// Parameters is only present for successful payments.
	//
	// Optional.
	Parameters mpesa.Parameters `json:"ResultParameters"`
}

// Result is a HTTP request body sent by M-Pesa to your ResultURL on success or fail.
type Result struct {

	// Result is the root parameter that encloses the entire result message.
	Result ResultData `json:"Result"`
}

// ID returns a unique M-PESA transaction ID.
func (r *Result) ID() string {
	return r.Result.ID
}

// CID returns the transaction's conversation ID.
func (r *Result) CID() string {
	return r.Result.CID
}

// OCID returns the transaction's originator conversation ID.
func (r *Result) OCID() string {
	return r.Result.OCID
}

// ResultType returns the type of the result.
//
// Result type is a status code that indicates whether the transaction was already
// sent to your listener. Usual value is 0.
func (r *Result) ResultType() int {
	return r.Result.Type
}

// ResultCode returns the transaction's result status code.
//
// Result status code is a numeric status code that indicates the status of the
// transaction processing.
func (r *Result) ResultCode() int {
	return r.Result.Code
}

// ResultDesc returns the
func (r *Result) ResultDesc() string {
	return r.Result.Desc
}

// Successful determines whether the transaction was successful or not.
func (r *Result) Successful() bool {
	return r.Result.Code == mpesa.ResultCodeSuccess
}

// Amount returns the M-Pesa transaction amount.
func (r *Result) Amount() (float64, error) {
	v, err := mpesa.Value(ParameterKeyTransactionAmount, r.Result.Parameters.Parameter)
	if err != nil {
		return 0, err
	}

	switch amt := v.(type) {
	case float64:
		return amt, nil
	case int:
		return float64(amt), nil
	default:
		return 0, mpesa.InvalidParameterValueError(ParameterKeyTransactionAmount, v)
	}
}

// TransactionReceipt returns a unique M-PESA transaction ID.
func (r *Result) TransactionReceipt() (string, error) {
	v, err := mpesa.Value(ParameterKeyTransactionReceipt, r.Result.Parameters.Parameter)
	if err != nil {
		return "", err
	}

	id, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ParameterKeyTransactionReceipt, v)
	}

	return id, nil
}

// B2CWorkingAccountAvailableFunds returns the available balance of B2C Working Account
// as of the time the result was sent by M-Pesa.
func (r *Result) B2CWorkingAccountAvailableFunds() (float64, error) {
	v, err := mpesa.Value(ParameterKeyB2CWorkingAccountAvailableFunds, r.Result.Parameters.Parameter)
	if err != nil {
		return 0.0, err
	}

	funds, ok := v.(float64)
	if !ok {
		return 0.0, mpesa.InvalidParameterValueError(ParameterKeyB2CWorkingAccountAvailableFunds, v)
	}

	return funds, nil
}

// B2CUtilityAccountAvailableFunds returns the available balance of B2C Utility Account
// as of the time the result was sent by M-Pesa.
func (r *Result) B2CUtilityAccountAvailableFunds() (float64, error) {
	v, err := mpesa.Value(ParameterKeyB2CUtilityAccountAvailableFunds, r.Result.Parameters.Parameter)
	if err != nil {
		return 0.0, err
	}

	funds, ok := v.(float64)
	if !ok {
		return 0.0, mpesa.InvalidParameterValueError(ParameterKeyB2CUtilityAccountAvailableFunds, v)
	}

	return funds, nil
}

// ChargesPaidAccountAvailableFunds returns the available balance of Charges Paid Account
// as of the time the result was sent by M-Pesa.
func (r *Result) ChargesPaidAccountAvailableFunds() (float64, error) {
	v, err := mpesa.Value(ParameterKeyB2CChargesPaidAccountAvailableFunds, r.Result.Parameters.Parameter)
	if err != nil {
		return 0.0, err
	}

	funds, ok := v.(float64)
	if !ok {
		return 0.0, mpesa.InvalidParameterValueError(ParameterKeyB2CChargesPaidAccountAvailableFunds, v)
	}

	return funds, nil
}

// CompletedDateTime returns the date and time the transaction was completed.
func (r *Result) CompletedDateTime() (string, error) {
	v, err := mpesa.Value(ParameterKeyTransactionCompletedDateTime, r.Result.Parameters.Parameter)
	if err != nil {
		return "", err
	}

	dt, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ParameterKeyTransactionCompletedDateTime, v)
	}

	return dt, nil
}

// ReceiverPartyPublicName returns the recipients mobile number and full name.
func (r *Result) ReceiverPartyPublicName() (*mpesa.Party, string, error) {
	v, err := mpesa.Value(ParameterKeyReceiverPartyPublicName, r.Result.Parameters.Parameter)
	if err != nil {
		return nil, "", err
	}

	name, ok := v.(string)
	if !ok {
		return nil, "", mpesa.InvalidParameterValueError(ParameterKeyReceiverPartyPublicName, v)
	}

	c, ok := mpesa.NewParty(name)
	if !ok {
		return nil, name, nil
	}

	return c, name, nil
}

// RecipientIsRegisteredCustomer determines whether the B2C recipient is a registered
// M-Pesa customer.
func (r *Result) RecipientIsRegisteredCustomer() (bool, error) {
	v, err := mpesa.Value(ParameterKeyB2CRecipientIsRegisteredCustomer, r.Result.Parameters.Parameter)
	if err != nil {
		return false, err
	}

	yesNo, ok := v.(string)
	if !ok {
		return false, mpesa.InvalidParameterValueError(ParameterKeyB2CRecipientIsRegisteredCustomer, v)
	}

	return strings.ToUpper(yesNo) == "Y", nil
}

// String returns a text format of Result by marshalling it into a JSON object.
func (r *Result) String() string {
	v, err := json.Marshal(r)
	if err != nil {
		return "{}"
	}

	return string(v)
}
