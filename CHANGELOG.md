### v1.7.0 2023/08/30

#### New features
- added date and time generator that uses the m-Pesa statements format and EAT timezone

### v1.6.3 2023/08/30

#### Bug fixes
- allow decimals on all APIs

### v1.6.2 2023/08/16

#### Bug fixes
- fixed b2c unit tests

### v1.6.1 2023/08/16

#### Bug fixes
- update the JSON key for b2b.Request#Initiator from InitiatorName to Initiator

### v1.6.0 2023/08/15

#### New features
- added String() function to mpesa.Response and mpesa.ErrorResponse
- added B2B api results HTTP body
- added Business buy goods and business paybill API integrations
- added B2B request data types

#### Enhancements
- added utility function ID() to b2c.Result and renamed other functions to relate to corresponding Result parameter key

### v1.5.1 2023/08/13

#### Bug fixes
- marked all params in mpesa.Kv as optional
- removed validation for mpesa.Kv

### v1.5.0 2023/08/11

#### New features
- defined all known mpesa result codes
- remove redundant PartyA and IdentifierType params from txquery.IOQuery and txquery.OCIDQuery. PartyA is the shortcode always and IdentifierType is 4 always
- added a dedicated b2c.Result to replace mpesa.Result
- added a dedicated txquery.Result to replace mpesa.Result

#### Enhancements
- renamed all txquery.Result functions

### v1.4.2 2023/08/08

#### Bug fixes
- fixed mpesa.Result struct validation caused by use of unknown tag length.

### v1.4.1 2023/08/08

#### New features
- updated b2c.API to use APIError for all unkown error responses
- defined a generic error (APIError) for use by all APIs to represent unknown M-Pesa API errors

#### Bug fixes
- updated b2c.API#sendMoney function to use the correct B2C api path

### v1.4.0 2023/08/07

#### Enhancements
- added a constructor for b2c.API

### v1.3.1 2023/08/07

#### Enhancements
- renamed mpesa.Customer to mpesa.ReceiverParty

#### Bug fixes
- updated ParseCertificate to check for nil block from pem.Decode()

### v1.3.0 2023/08/07

#### Enhancements
- Added MIT license

### v1.2.0 2023/08/06

#### New features
- renamed defaultSCEncryptor to DefaultSCEncryptor
- deprecated NewDefaultSCEncryptor and added ParseCertificate and NewSCEncryptor functions
- updated txquery.API functions to return both the request and response data to caller
- updated b2c.API functions to return both the request and response data to caller
- used sigleton SCEncryptor

#### Enhancements
- renamed b2c.RequiredInfo to b2c.SendMoneyInfo
- renamed express.PaybillOnline and express.BuyGoodsOnline to express.OnlineBillpayInfo and express.OnlineBuyGoodsInfo respectively
- documented txquery.IDQuery and txquery.OCIDQuery
- renamed defaultSCEncryptor to DefaultSCEncryptor

### v1.0.2 2023/08/04

#### New features
- updated express.Txn to include Successful() and TxnSuccessful() functions.

#### Bug fixes
- updated express.Response#Code to type scrting from int

### v1.0.0 2023/08/04

#### New features
- added M-Pesa Express Buy goods and Paybill online API integrations
- added B2C salary payment, promotion payment and business payment API integrations
- added an in-memory store for M-Pesa initiator credentials
- added Txn status query by ID and Txn status query by originator conversation ID API integrations
- added a security credential RSA encryptor
- added m-pesa api paths and command ids
- added a token expiry listener
- added inmemory app store
- added an inmemory store for access tokens
- added inmemory OAuth token store
- added OAuth API integration
- added the 2 main Safaricom api environments: Sandbox and Prod
- added an interceptor for logging HTTP requests and responses
- added HTTP request interceptor and interceptor chain spec
- added HTTP request Matchers
- added basic HTTP client GET and POST implementations
- updated b2c.Result#ReceiverPartyPublicName() to return Customer data
- added struct validation using Go Validator and all unit tests for result.go and response.go
- added b2c.Result type with all required functions for handling callbacks with ease
- added and validated b2c.Response and b2c.ErrorResponse - this is the synchronous response returned by M-pesa on fail/success
- defined and validated b2c.Request - this is the request body spec for Daraja B2C RESTful API