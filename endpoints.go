package mpesa

const (
	// EnvSafaricomSandbox is the host name for all Safaricon sandbox APIs.
	EnvSafaricomSandbox = "https://sandbox.safaricom.co.ke"

	// EnvSafaricomProd is the host name for all Safaricon production APIs.
	EnvSafaricomProd = "https://api.safaricom.co.ke"

	// PathOAuth is the HTTP path for Safaricom's OAuth Token endpoint.
	PathOAuth = "/oauth/v1/generate?grant_type=client_credentials"

	// PathB2C is the HTTP path for Safaricom's M-Pesa B2C v1 API.
	PathB2C = "/mpesa/b2c/v1/paymentrequest"

	// PathDynamicQRCode is the HTTP path for Safaricom's M-Pesa Dynamic QR Code v1 API.
	PathDynamicQRCode = "/mpesa/qrcode/v1/generate"

	// PathSTKPushQuery is the HTTP path for Safaricom's M-Pesa Express Status Query v1 API.
	PathSTKPushQuery = "/mpesa/stkpushquery/v1/query"

	// PathSTKPushProcess is the HTTP path for Safaricom's M-Pesa Express v1 API.
	PathSTKPushProcess = "/mpesa/stkpush/v1/processrequest"

	// PathB2B is the HTTP path for Safaricom's M-Pesa B2B v1 API.
	PathB2B = "/mpesa/b2b/v1/paymentrequest"

	// PathTxnStatusQuery is the HTTP path for Safaricom's Txn Status Query v1 API.
	PathTxnStatusQuery = "/mpesa/transactionstatus/v1/query"

	// PathReversal is the HTTP path for Safaricom's M-Pesa Reversal v1 API.
	PathReversal = "/mpesa/reversal/v1/request"

	// PathTaxRemittance is the HTTP path for Safaricom's M-Pesa KRA Tax Remittance v1 API.
	PathTaxRemittance = "/mpesa/b2b/v1/remittax"
)
