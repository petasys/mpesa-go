package mpesa

import (
	"bytes"
	"io"
	"log"
	"net/http"
	"strings"
	"time"
)

const (
	HTTPLoggingLevelNone int = iota
	HTTPLoggingLevelBasic
	HTTPLoggingLevelHeaders
	HTTPLoggingLevelBody
)

type HTTPLoggingInterceptor struct {
	matcher Matcher
	level   int
}

func (i *HTTPLoggingInterceptor) Matcher() Matcher {
	return i.matcher
}

func (i *HTTPLoggingInterceptor) Intercept(chain Chain, req *http.Request) (*http.Response, error) {
	if i.level <= HTTPLoggingLevelNone {
		return chain.Proceed(req)
	}

	start := time.Now()
	b := readRequestBody(req)
	log.Printf("--> %s %s %s (%d-byte body)", req.Method, req.URL, req.Proto, len(b))
	if i.level >= HTTPLoggingLevelHeaders {
		logHeaders(req.Header)
	}

	if i.level >= HTTPLoggingLevelBody {
		log.Println()
		log.Println(string(b))
	}

	log.Print("--> END\n\n")

	res, err := chain.Proceed(req)
	if err != nil {
		log.Println("<-- HTTP FAILED:", err)
		return res, err
	}

	b = readResponseBody(res)
	resTime := time.Since(start).Milliseconds()
	log.Printf("<-- %s (%dms, %d-byte body)", res.Status, resTime, len(b))

	if i.level >= HTTPLoggingLevelHeaders {
		logHeaders(res.Header)
	}

	if i.level >= HTTPLoggingLevelBody {
		log.Println()
		log.Println(string(b))
	}

	log.Println("<-- END HTTP")
	return res, nil
}

func logHeaders(hs http.Header) {
	for k, v := range hs {
		log.Println(k, ": ", strings.Join(v, ", "))
	}
}

func readRequestBody(r *http.Request) []byte {
	if r.Body == nil {
		return nil
	}

	b, err := io.ReadAll(r.Body)
	if err != nil {
		return nil
	}

	r.Body = io.NopCloser(bytes.NewBuffer(b))
	return b
}

func readResponseBody(r *http.Response) []byte {
	if r.Body == nil {
		return nil
	}

	b, err := io.ReadAll(r.Body)
	if err != nil {
		return nil
	}

	r.Body = io.NopCloser(bytes.NewBuffer(b))
	return b
}

func NewHTTPLoggingInterceptor(matcher Matcher, level int) *HTTPLoggingInterceptor {
	return &HTTPLoggingInterceptor{matcher: matcher, level: level}
}
