package mpesa

import (
	"crypto/rand"
	"crypto/rsa"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"errors"
	"io"
	"os"
	"sync"
)

var encryptor SCEncryptor
var encryptorLock = &sync.Mutex{}

// Initiator is a M-Pesa API operator used to do B2C and/or B2B transactions.
type Initiator interface {

	// Shortcode returns the unique number that is allocated to a pay bill or buy goods
	// organization through they will be able to receive customer payment.
	//
	// It could be a Pay bill, Buy Goods or Till Number.
	Shortcode() string

	// Name returns the username of the M-Pesa API operator. NOTE: the access channel
	// for this operator myst be API and the account must be in active status.
	Name() string

	// Password returns the password of the M-Pesa API operator.
	Password() string
}

// InitiatorStore is a data access API for managing M-Pesa transaction initiator
// credentials.
type InitiatorStore interface {

	// Read returns a single Initiator given the shortcode.
	//
	// An error is returned if the Initiator could not be found or is another
	// store related error occurs.
	Read(shortcode string) (Initiator, error)

	// ReadAll fetches all stored Initiators and returns them in an array.
	//
	// An error is returned if the underlying store fails.
	ReadAll() ([]Initiator, error)

	// Write registers a new Initiator or updates an existing Initiator, given
	// the shortcode, name and password.
	//
	// An error is returned if either the shortcode, the name or the password is blank
	// or if the underlying store returns an error.
	Write(shortcode string, name string, pwd string) error

	// Delete removes an Initiator from the store.
	//
	// An error is returned if the underlying store fails.
	Delete(shortcode string) error
}

// SCEncryptor is an RSA encryptor which encrypts security credentials with the
// M-Pesa public key certificate.
type SCEncryptor interface {

	// PublicKey returns a RSA public key issued by M-Pesa for each environment.
	PublicKey() *rsa.PublicKey

	// Encrypt is used to encrypt the Security Credentials for the following APIs:
	//   - B2C
	//   - B2B
	//   - Transaction Status Query API
	//   - Reversal API.
	//
	// The resulting encrypted byte array is converted into a string using base64 encoding.
	//
	// The resulting base64 encoded string is the security credential.
	Encrypt(init string) (string, error)
}

// Encrypt is used to encrypt the Security Credentials for the following APIs:
//   - B2C
//   - B2B
//   - Transaction Status Query API
//   - Reversal API.
//
// The resulting encrypted byte array is converted into a string using base64 encoding.
//
// The resulting base64 encoded string is the security credential.

// defaultInitiator is a trivial implementation of Initiator used for in-memory
// storage.
type defaultInitiator struct {
	shortcode string
	name      string
	password  string
}

type defaultSCEncryptor struct {
	pub *rsa.PublicKey
}

type InmemoryInitiatorStore struct {
	inits map[string]defaultInitiator
}

func (i *defaultInitiator) Shortcode() string {
	return i.shortcode
}

func (i *defaultInitiator) Name() string {
	return i.name
}

func (i *defaultInitiator) Password() string {
	return i.password
}

func (s *InmemoryInitiatorStore) Read(shortcode string) (Initiator, error) {
	init, ok := s.inits[shortcode]
	if !ok {
		return nil, errors.New("access token not found")
	}

	return &init, nil
}

func (s *InmemoryInitiatorStore) ReadAll() ([]Initiator, error) {
	inits := make([]Initiator, 0, len(s.inits))
	for _, v := range s.inits {
		inits = append(inits, &v)
	}

	return inits, nil
}

func (s *InmemoryInitiatorStore) Write(shortcode string, name string, pwd string) error {
	if len(shortcode) == 0 {
		return errors.New("shortcode is blank")
	}

	if len(name) == 0 {
		return errors.New("initiator name is blank")
	}

	if len(pwd) == 0 {
		return errors.New("initiator password is blank")
	}

	init := defaultInitiator{shortcode: shortcode, name: name, password: pwd}
	s.inits[shortcode] = init
	return nil
}

func (enc *defaultSCEncryptor) PublicKey() *rsa.PublicKey {
	return enc.pub
}

func (enc *defaultSCEncryptor) Encrypt(init string) (string, error) {
	encrypted, err := rsa.EncryptPKCS1v15(rand.Reader, enc.pub, []byte(init))
	if err != nil {
		return "", err
	}

	return base64.StdEncoding.EncodeToString(encrypted), nil
}

func (s *InmemoryInitiatorStore) Delete(shortcode string) error {
	delete(s.inits, shortcode)
	return nil
}

func NewInmemoryInitiatorStore() *InmemoryInitiatorStore {
	return &InmemoryInitiatorStore{inits: map[string]defaultInitiator{}}
}

// ParseCertificate extracts RSA public key from a certificate file.
func ParseCertificate(certFile string) (*rsa.PublicKey, error) {
	f, err := os.OpenFile(certFile, 0, os.ModeAppend)
	if err != nil {
		return nil, err
	}

	defer f.Close()

	fileData, err := io.ReadAll(f)
	if err != nil {
		return nil, err
	}

	block, _ := pem.Decode(fileData)
	if block == nil {
		return nil, errors.New("no PEM block found in file data")
	}

	pub, err := x509.ParseCertificate(block.Bytes)
	if err != nil {
		return nil, err
	}

	rsaPub, ok := pub.PublicKey.(*rsa.PublicKey)
	if !ok {
		return nil, errors.New("file data not RSA public key")
	}

	return rsaPub, nil
}

// SCEncryptorInstance initializes the global SCEncryptor and returns a pointer
// to the defaultSCEncryptor.
//
// Multiple calls to this method have no impact.
func SCEncryptorInstance(pub *rsa.PublicKey) (SCEncryptor, error) {
	if encryptor == nil {
		if pub == nil {
			return nil, errors.New("public key is Nil")
		}

		encryptorLock.Lock()
		defer encryptorLock.Unlock()

		encryptor = &defaultSCEncryptor{pub: pub}
	}

	return encryptor, nil
}
