package mpesa

const (
	CommandIDSalaryPayment          = "SalaryPayment"
	CommandIDBusinessPayment        = "BusinessPayment"
	CommandIDPromotionPayment       = "PromotionPayment"
	CommandIDTransactionStatusQuery = "TransactionStatusQuery"
	CommandIDBusinessPayBill        = "BusinessPayBill"
	CommandIDBusinessBuyGoods       = "BusinessBuyGoods"

	TransactionTypeCustomerPayBillOnline  = "CustomerPayBillOnline"
	TransactionTypeCustomerBuyGoodsOnline = "CustomerBuyGoodsOnline"

	// MSISDN.
	IdentifierTypeMSISDN = 1

	// Till number.
	IdentifierTypeTillNumber = 2

	// Organization short code.
	IdentifierTypeOrgShortcode = 4
)
