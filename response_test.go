package mpesa

import (
	"fmt"
	"testing"
)

func TestResponseSuccessful(t *testing.T) {
	tests := []struct {
		name string
		code string
		want bool
	}{
		{name: "FailedIfCodeIsNotZero", code: "2001", want: false},
		{name: "SuccessfulIfCodeIsZero", code: "0", want: true},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			r := Response{Code: test.code}
			if got := r.Successful(); got != test.want {
				t.Errorf("Successful() = %v; want = %v", got, test.want)
			}
		})
	}
}

func TestErrorResponseError(t *testing.T) {
	r := ErrorResponse{ID: "ID", Code: "Code", Message: "Message"}
	want := fmt.Sprintf("{requestId: %s,errorCode: %s, errorMessage: %s}", r.ID, r.Code, r.Message)
	if got := r.Error(); got != want {
		t.Errorf("Error() = %v; want = %v", got, want)
	}

}
