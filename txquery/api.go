// Copyright 2023 M-Pesa Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// The Transaction Status API is used to check the status of a transaction.
package txquery

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"

	"github.com/go-playground/validator/v10"
	"gitlab.com/petasys/mpesa-go"
)

type API struct {
	// env is the B2C API host name.
	env string

	// client is the underlying HTTP client with support for interceptors e.t.c.
	client mpesa.Client

	// tknStore is a store where the access token is retrived for each transaction.
	tknStore mpesa.TokenStore

	// initStore is a store where the M-Pesa initiator name and password are stored.
	initStore mpesa.InitiatorStore

	// encryptor is an API for encrypting initiator password using RSA pub key and
	// encoding the result using base64.
	encryptor mpesa.SCEncryptor

	// validate is used to run validation checks for all request params. The idea is to
	// cancel an invalid request before it makes it to M-Pesa.
	validate *validator.Validate
}

func (api *API) query(b *Request, tkn mpesa.Token) (*mpesa.Response, error) {
	bBytes, err := json.Marshal(&b)
	if err != nil {
		return nil, err
	}

	url := api.env + mpesa.PathTxnStatusQuery
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(bBytes))
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", "Bearer "+tkn.Token())
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	res, err := api.client.Post(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		var errBody mpesa.ErrorResponse
		if err := json.NewDecoder(res.Body).Decode(&errBody); err != nil {
			return nil, err
		}

		return nil, &errBody
	}

	var successBody mpesa.Response
	if err := json.NewDecoder(res.Body).Decode(&successBody); err != nil {
		return nil, err
	}

	return &successBody, nil
}

func (api *API) QueryByID(shortcode string, q *IDQuery) (*Request, *mpesa.Response, error) {
	if err := api.validate.Struct(q); err != nil {
		return nil, nil, err
	}

	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	initr, err := api.initStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	credential, err := api.encryptor.Encrypt(initr.Password())
	if err != nil {
		return nil, nil, err
	}

	b := Request{
		CommandID:       mpesa.CommandIDTransactionStatusQuery,
		ID:              q.ID,
		IdentifierType:  mpesa.IdentifierTypeOrgShortcode,
		PartyA:          shortcode,
		Initiator:       initr.Name(),
		Credential:      credential,
		ResultURL:       q.ResultURL,
		QueueTimeoutURL: q.QueueTimeoutURL,
		Remarks:         q.Remarks,
		Occassion:       q.Occassion,
	}

	res, err := api.query(&b, tkn)
	return &b, res, err
}

func (api *API) QueryByOCID(shortcode string, q *OCIDQuery) (*Request, *mpesa.Response, error) {
	if err := api.validate.Struct(q); err != nil {
		return nil, nil, err
	}

	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	init, err := api.initStore.Read(shortcode)
	if err != nil {
		return nil, nil, err
	}

	credential, err := api.encryptor.Encrypt(init.Password())
	if err != nil {
		return nil, nil, err
	}

	b := Request{
		CommandID:       mpesa.CommandIDTransactionStatusQuery,
		OCID:            q.OCID,
		IdentifierType:  mpesa.IdentifierTypeOrgShortcode,
		PartyA:          shortcode,
		Initiator:       init.Name(),
		Credential:      credential,
		ResultURL:       q.ResultURL,
		QueueTimeoutURL: q.QueueTimeoutURL,
		Remarks:         q.Remarks,
		Occassion:       q.Occassion,
	}

	res, err := api.query(&b, tkn)
	return &b, res, err
}

func New(env string, clt mpesa.Client, ts mpesa.TokenStore, is mpesa.InitiatorStore, v *validator.Validate,
	enc mpesa.SCEncryptor) (*API, error) {

	if len(env) == 0 {
		return nil, errors.New("env is blank")
	}

	if clt == nil {
		return nil, errors.New("client is Nil")
	}

	if ts == nil {
		return nil, errors.New("token store is Nil")
	}

	if is == nil {
		return nil, errors.New("initiator store is Nil")
	}

	if v == nil {
		return nil, errors.New("validator is Nil")
	}

	if enc == nil {
		return nil, errors.New("security credentials encryptor is Nil")
	}

	return &API{env: env, client: clt, tknStore: ts, initStore: is, validate: v, encryptor: enc}, nil
}
