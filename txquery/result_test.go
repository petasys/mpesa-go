package txquery

// import (
// 	"fmt"
// 	"testing"

// 	"github.com/go-playground/validator/v10"
// )

// func TestNewCustomer(t *testing.T) {
// 	tests := []struct {
// 		name string
// 		v    string
// 		ok   bool
// 		c    *ReceiverParty
// 	}{
// 		{
// 			name: "FailToNewCustomer",
// 			v:    "",
// 			ok:   false,
// 		},
// 		{
// 			name: "NewCustomerWith10DigitPhone",
// 			v:    "0722000000 - Safaricom PLC",
// 			ok:   true,
// 			c:    &ReceiverParty{Name: "Safaricom PLC", Shortcode: "254722000000"},
// 		},
// 		{
// 			name: "NewCustomerWith12DigitPhone",
// 			v:    "254722000000 - Safaricom PLC",
// 			ok:   true,
// 			c:    &ReceiverParty{Name: "Safaricom PLC", Shortcode: "254722000000"},
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			c, ok := newReceiverParty(test.v)
// 			if ok != test.ok {
// 				t.Errorf("newCustomer(%s) = (%v,%v); want = (%v,%v)", test.v, c, ok, test.c, test.ok)
// 			}

// 			if c == nil {
// 				return
// 			}

// 			if c.Name != test.c.Name {
// 				t.Errorf("newCustomer(%s) = (%v,%v); want = (%v,%v)", test.v, c, ok, test.c, test.ok)
// 			}

// 			if c.Shortcode != test.c.Shortcode {
// 				t.Errorf("newCustomer(%s) = (%v,%v); want = (%v,%v)", test.v, c, ok, test.c, test.ok)
// 			}
// 		})
// 	}
// }

// func TestCustomerString(t *testing.T) {
// 	c := &ReceiverParty{Name: "Safaricom PLC", Shortcode: "254722000000"}
// 	want := "254722000000 - Safaricom PLC"
// 	if got := c.String(); got != want {
// 		t.Errorf("String() = %s; want = %s", got, want)
// 	}
// }

// func TestResultCID(t *testing.T) {
// 	r := Result{Result: ResultData{CID: "CID"}}
// 	if got := r.CID(); got != "CID" {
// 		t.Errorf("CID() = %s; want = CID", got)
// 	}
// }

// func TestResultOCID(t *testing.T) {
// 	r := Result{Result: ResultData{OCID: "OCID"}}
// 	if got := r.OCID(); got != "OCID" {
// 		t.Errorf("OCID() = %s; want = OCID", got)
// 	}
// }

// func TestResultType(t *testing.T) {
// 	r := Result{Result: ResultData{Type: 0}}
// 	if got := r.ResultType(); got != 0 {
// 		t.Errorf("ResultType() = %d; want = 0", got)
// 	}
// }

// func TestResultCode(t *testing.T) {
// 	r := Result{Result: ResultData{Code: 0}}
// 	if got := r.ResultCode(); got != 0 {
// 		t.Errorf("ResultCode() = %d; want = 0", got)
// 	}
// }

// func TestResultDesc(t *testing.T) {
// 	r := Result{Result: ResultData{Desc: "ResultDesc"}}
// 	if got := r.ResultDesc(); got != "ResultDesc" {
// 		t.Errorf("ResultDesc() = %s; want = ResultDesc", got)
// 	}
// }

// func TestResultSuccessful(t *testing.T) {
// 	tests := []struct {
// 		name string
// 		code int
// 		want bool
// 	}{
// 		{name: "FailedIfCodeIsNotZero", code: 2001, want: false},
// 		{name: "SuccessfulIfCodeIsZero", code: 0, want: true},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Code: test.code}}
// 			if got := r.Successful(); got != test.want {
// 				t.Errorf("Successful() = %v; want = %v", got, test.want)
// 			}
// 		})
// 	}
// }

// func TestResultParameterValue(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		params []Kv
// 		k      string
// 		v      interface{}
// 		err    string
// 	}{
// 		{
// 			name:   "FailToResultParameterValueIfListIsEmpty",
// 			params: []Kv{},
// 			k:      ParameterKeyTransactionAmount,
// 			err:    "ResultParameter TransactionAmount not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToResultParameterValueIfListIsNil",
// 			params: nil,
// 			k:      ParameterKeyTransactionAmount,
// 			err:    "ResultParameter TransactionAmount not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToResultParameterValueIfKNotFound",
// 			params: []Kv{{K: ParameterKeyTransactionReceipt, V: ""}},
// 			k:      ParameterKeyTransactionAmount,
// 			err:    "ResultParameter TransactionAmount not found",
// 		},
// 		{
// 			name:   "ResultParameterValue",
// 			params: []Kv{{K: ParameterKeyTransactionAmount, V: 0}},
// 			k:      ParameterKeyTransactionAmount,
// 			v:      0,
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Parameters: Parameters{Parameter: test.params}}}
// 			v, err := r.resultParameterValue(test.k)
// 			if err != nil {
// 				if err.Error() != test.err {
// 					t.Errorf("resultParameterValue(%s) = (%v,%v); want = (%v,%s)", test.k, v, err, test.v, test.err)
// 				}
// 				return
// 			}

// 			if v != test.v {
// 				t.Errorf("resultParameterValue(%s) = (%v,%v); want = (%v,%s)", test.k, v, err, test.v, test.err)
// 			}
// 		})
// 	}
// }

// func TestResultAmount(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		params []Kv
// 		v      interface{}
// 		err    string
// 	}{
// 		{
// 			name:   "FailToAmountIfResultParameterValueFails",
// 			params: []Kv{},
// 			err:    "ResultParameter TransactionAmount not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToAmountIfParameterTransactionAmountIsNotInteger",
// 			params: []Kv{{K: ParameterKeyTransactionAmount, V: "100.00"}},
// 			err:    invalidParameterValueError(ParameterKeyTransactionAmount, "100.00").Error(),
// 		},
// 		{
// 			name:   "Amount",
// 			params: []Kv{{K: ParameterKeyTransactionAmount, V: 100}},
// 			v:      100,
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Parameters: Parameters{Parameter: test.params}}}
// 			v, err := r.Amount()
// 			if err != nil {
// 				if err.Error() != test.err {
// 					t.Errorf("Amount() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 				}
// 				return
// 			}

// 			if v != test.v {
// 				t.Errorf("Amount() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 			}
// 		})
// 	}
// }

// func TestResultReceipt(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		id     string
// 		params []Kv
// 		v      interface{}
// 		err    string
// 	}{
// 		{
// 			name:   "FailToReceiptIfResultParameterValueFails",
// 			params: []Kv{},
// 			err:    "ResultParameter TransactionReceipt not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToReceiptIfParameterTransactionReceiptIsNotString",
// 			params: []Kv{{K: ParameterKeyTransactionReceipt, V: 100}},
// 			err:    invalidParameterValueError(ParameterKeyTransactionReceipt, 100).Error(),
// 		},
// 		{
// 			name:   "ReceiptAsParameterKeyTransactionReceipt",
// 			params: []Kv{{K: ParameterKeyTransactionReceipt, V: "Receipt"}},
// 			v:      "Receipt",
// 		},
// 		{
// 			name: "ReceiptAsID",
// 			id:   "Receipt",
// 			v:    "Receipt",
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{ID: test.id, Parameters: Parameters{Parameter: test.params}}}
// 			v, err := r.Receipt()
// 			if err != nil {
// 				if err.Error() != test.err {
// 					t.Errorf("Amount() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 				}
// 				return
// 			}

// 			if v != test.v {
// 				t.Errorf("Amount() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 			}
// 		})
// 	}
// }

// func TestResultWorkingAccountAvailableFunds(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		params []Kv
// 		v      interface{}
// 		err    string
// 	}{
// 		{
// 			name:   "FailToWorkingAccountAvailableFundsIfResultParameterValueFails",
// 			params: []Kv{},
// 			err:    "ResultParameter B2CWorkingAccountAvailableFunds not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToWorkingAccountAvailableFundsIfVIsNotFloat",
// 			params: []Kv{{K: ParameterKeyB2CWorkingAccountAvailableFunds, V: "100.00"}},
// 			err:    invalidParameterValueError(ParameterKeyB2CWorkingAccountAvailableFunds, "100.00").Error(),
// 		},
// 		{
// 			name:   "WorkingAccountAvailableFunds",
// 			params: []Kv{{K: ParameterKeyB2CWorkingAccountAvailableFunds, V: 100.0}},
// 			v:      100.0,
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Parameters: Parameters{Parameter: test.params}}}
// 			v, err := r.WorkingAccountAvailableFunds()
// 			if err != nil {
// 				if err.Error() != test.err {
// 					t.Errorf("WorkingAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 				}
// 				return
// 			}

// 			if v != test.v {
// 				t.Errorf("WorkingAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 			}
// 		})
// 	}
// }

// func TestResultUtilityAccountAvailableFunds(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		params []Kv
// 		v      interface{}
// 		err    string
// 	}{
// 		{
// 			name:   "FailToUtilityAccountAvailableFundsIfResultParameterValueFails",
// 			params: []Kv{},
// 			err:    "ResultParameter B2CUtilityAccountAvailableFunds not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToUtilityAccountAvailableFundsIfVIsNotFloat",
// 			params: []Kv{{K: ParameterKeyB2CUtilityAccountAvailableFunds, V: "100.00"}},
// 			err:    invalidParameterValueError(ParameterKeyB2CUtilityAccountAvailableFunds, "100.00").Error(),
// 		},
// 		{
// 			name:   "UtilityAccountAvailableFunds",
// 			params: []Kv{{K: ParameterKeyB2CUtilityAccountAvailableFunds, V: 100.0}},
// 			v:      100.0,
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Parameters: Parameters{Parameter: test.params}}}
// 			v, err := r.UtilityAccountAvailableFunds()
// 			if err != nil {
// 				if err.Error() != test.err {
// 					t.Errorf("UtilityAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 				}
// 				return
// 			}

// 			if v != test.v {
// 				t.Errorf("UtilityAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 			}
// 		})
// 	}
// }

// func TestResultChargesPaidAccountAvailableFunds(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		params []Kv
// 		v      interface{}
// 		err    string
// 	}{
// 		{
// 			name:   "FailToChargesPaidAccountAvailableFundsIfResultParameterValueFails",
// 			params: []Kv{},
// 			err:    "ResultParameter B2CChargesPaidAccountAvailableFunds not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToChargesPaidAccountAvailableFundsIfVIsNotFloat",
// 			params: []Kv{{K: ParameterKeyB2CChargesPaidAccountAvailableFunds, V: "100.00"}},
// 			err:    invalidParameterValueError(ParameterKeyB2CChargesPaidAccountAvailableFunds, "100.00").Error(),
// 		},
// 		{
// 			name:   "ChargesPaidAccountAvailableFunds",
// 			params: []Kv{{K: ParameterKeyB2CChargesPaidAccountAvailableFunds, V: 100.0}},
// 			v:      100.0,
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Parameters: Parameters{Parameter: test.params}}}
// 			v, err := r.ChargesPaidAccountAvailableFunds()
// 			if err != nil {
// 				if err.Error() != test.err {
// 					t.Errorf("ChargesPaidAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 				}
// 				return
// 			}

// 			if v != test.v {
// 				t.Errorf("ChargesPaidAccountAvailableFunds() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 			}
// 		})
// 	}
// }

// func TestResultCompletedDateTime(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		params []Kv
// 		v      interface{}
// 		err    string
// 	}{
// 		{
// 			name:   "FailToCompletedDateTimeIfResultParameterValueFails",
// 			params: []Kv{},
// 			err:    "ResultParameter TransactionCompletedDateTime not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToCompletedDateTimeIfCompletedDateTimeIsNotString",
// 			params: []Kv{{K: ParameterKeyTransactionCompletedDateTime, V: 100}},
// 			err:    invalidParameterValueError(ParameterKeyTransactionCompletedDateTime, 100).Error(),
// 		},
// 		{
// 			name:   "CompletedDateTime",
// 			params: []Kv{{K: ParameterKeyTransactionCompletedDateTime, V: "CompletedDateTime"}},
// 			v:      "CompletedDateTime",
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Parameters: Parameters{Parameter: test.params}}}
// 			v, err := r.CompletedDateTime()
// 			if err != nil {
// 				if err.Error() != test.err {
// 					t.Errorf("CompletedDateTime() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 				}
// 				return
// 			}

// 			if v != test.v {
// 				t.Errorf("CompletedDateTime() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 			}
// 		})
// 	}
// }

// func TestResultReceiverPartyPublicName(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		params []Kv
// 		v      interface{}
// 		err    string
// 		c      *ReceiverParty
// 	}{
// 		{
// 			name:   "FailToReceiverPartyPublicNameIfResultParameterValueFails",
// 			params: []Kv{},
// 			err:    "ResultParameter ReceiverPartyPublicName not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToReceiverPartyPublicNameIfVIsNotString",
// 			params: []Kv{{K: ParameterKeyReceiverPartyPublicName, V: 100}},
// 			err:    invalidParameterValueError(ParameterKeyReceiverPartyPublicName, 100).Error(),
// 		},
// 		{
// 			name:   "ReceiverPartyPublicNameWithoutCustomer",
// 			params: []Kv{{K: ParameterKeyReceiverPartyPublicName, V: "0070000000Test Test"}},
// 			v:      "0070000000Test Test",
// 		},
// 		{
// 			name:   "ReceiverPartyPublicNameWithCustomer",
// 			params: []Kv{{K: ParameterKeyReceiverPartyPublicName, V: "0070000000 - Test Test"}},
// 			v:      "0070000000 - Test Test",
// 			c:      &ReceiverParty{Name: "Test Test", Shortcode: "254070000000"},
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Parameters: Parameters{Parameter: test.params}}}
// 			c, v, err := r.ReceiverPartyPublicName()
// 			if err != nil {
// 				if err.Error() != test.err {
// 					t.Errorf("ReceiverPartyPublicName() = (%v,%v,%v); want = (%v,%s)", c, v, err, test.v, test.err)
// 				}
// 				return
// 			}

// 			if v != test.v {
// 				t.Errorf("ReceiverPartyPublicName() = (%v,%v,%v); want = (%v,%s)", c, v, err, test.v, test.err)
// 			}

// 			if c == nil {
// 				return
// 			}

// 			if c.Name != test.c.Name {
// 				t.Errorf("ReceiverPartyPublicName() = (%v,%v,%v); want = (%v,%s)", c, v, err, test.v, test.err)
// 			}

// 			if c.Shortcode != test.c.Shortcode {
// 				t.Errorf("ReceiverPartyPublicName() = (%v,%v,%v); want = (%v,%s)", c, v, err, test.v, test.err)
// 			}

// 		})
// 	}
// }

// func TestResultRecipientIsRegisteredCustomer(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		params []Kv
// 		v      interface{}
// 		err    string
// 	}{
// 		{
// 			name:   "FailToRecipientIsRegisteredCustomerIfResultParameterValueFails",
// 			params: []Kv{},
// 			err:    "ResultParameter B2CRecipientIsRegisteredCustomer not found. ResultParameter list is either Nil or empty",
// 		},
// 		{
// 			name:   "FailToRecipientIsRegisteredCustomerIfVIsNotString",
// 			params: []Kv{{K: ParameterKeyB2CRecipientIsRegisteredCustomer, V: 100}},
// 			err:    invalidParameterValueError(ParameterKeyB2CRecipientIsRegisteredCustomer, 100).Error(),
// 		},
// 		{
// 			name:   "RecipientIsRegisteredCustomer",
// 			params: []Kv{{K: ParameterKeyB2CRecipientIsRegisteredCustomer, V: "Y"}},
// 			v:      true,
// 		},
// 		{
// 			name:   "RecipientIsUnregisteredCustomer",
// 			params: []Kv{{K: ParameterKeyB2CRecipientIsRegisteredCustomer, V: "N"}},
// 			v:      false,
// 		},
// 	}

// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Parameters: Parameters{Parameter: test.params}}}
// 			v, err := r.RecipientIsRegisteredCustomer()
// 			if err != nil {
// 				if err.Error() != test.err {
// 					t.Errorf("RecipientIsRegisteredCustomer() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 				}
// 				return
// 			}

// 			if v != test.v {
// 				t.Errorf("RecipientIsRegisteredCustomer() = (%v,%v); want = (%v,%s)", v, err, test.v, test.err)
// 			}
// 		})
// 	}
// }

// func TestResultString(t *testing.T) {
// 	tests := []struct {
// 		name   string
// 		params []Kv
// 	}{
// 		{name: "StringIfJSONMarshalFails", params: []Kv{{K: "", V: make(chan int)}}},
// 		{name: "String", params: []Kv{}},
// 	}

// 	v := validator.New()
// 	for _, test := range tests {
// 		t.Run(test.name, func(t *testing.T) {
// 			r := Result{Result: ResultData{Parameters: Parameters{Parameter: test.params}}}
// 			text := r.String()
// 			if err := v.Var(text, "json"); err != nil {
// 				t.Errorf("String() = %s; want = JSON object", text)
// 			}
// 		})
// 	}
// }

// func TestInvalidParameterValueError(t *testing.T) {
// 	want := fmt.Errorf("%v is an invalid %s result parameter value", 0, ParameterKeyTransactionAmount)
// 	got := invalidParameterValueError(ParameterKeyTransactionAmount, 0)
// 	if want.Error() != got.Error() {
// 		t.Errorf("invalidParameterValueError(%s,%v) = %s; want = %s", ParameterKeyTransactionAmount, 0, got, want)
// 	}
// }
