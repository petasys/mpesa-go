package txquery

import (
	"encoding/json"

	"gitlab.com/petasys/mpesa-go"
)

const (
	TransactionStatusCompleted = "Completed"
	TransactionStatusCancelled = "Cancelled"

	// ParameterKeyDebitPartyName is the short code and the name of sender of the
	// money.
	//
	// A short code, in this case, can be a mobile number or an organization short
	// code.
	ParameterKeyDebitPartyName = "DebitPartyName"

	// ParameterKeyCreditPartyName is the short code and the name of recipient of
	// the money.
	//
	// A short code, in this case, can be a mobile number or an organization short
	// code.
	ParameterKeyCreditPartyName = "CreditPartyName"

	// ParameterKeyOriginatorConversationID is the original OriginatorConversationID
	// received from M-Pesa during the submission of the payment request.
	ParameterKeyOriginatorConversationID = "OriginatorConversationID"

	// ParameterKeyConversationID is the original ConversationID received from
	// M-Pesa during the submission of the payment request.
	ParameterKeyConversationID = "ConversationID"

	// ParameterKeyInitiatedTime is the date and time the payment request was
	// initiated.
	ParameterKeyInitiatedTime = "InitiatedTime"

	// ParameterKeyDebitAccountType is the name of the type of the sender account.
	ParameterKeyDebitAccountType = "DebitAccountType"

	// ParameterKeyDebitPartyCharges are the charges incurred by the sender.
	ParameterKeyDebitPartyCharges = "DebitPartyCharges"

	// ParameterKeyReasonType is the payment type.
	ParameterKeyReasonType = "ReasonType"

	// ParameterKeyTransactionStatus is the status of the payment for which the
	// status query was done.
	ParameterKeyTransactionStatus = "TransactionStatus"

	// ParameterKeyFinalisedTime is the date and time the payment was finalised.
	ParameterKeyFinalisedTime = "FinalisedTime"

	// ParameterKeyAmount is the transaction amount.
	ParameterKeyAmount = "Amount"

	// ParameterKeyReceiptNo is a unique M-PESA transaction ID for every payment
	// request.
	//
	// Same value is sent to customer over SMS upon successful processing e.g. LHG31AA5TX.
	ParameterKeyReceiptNo = "ReceiptNo"
)

// Reference is JSON object that holds more details about the result.
type ReferenceData struct {
	Item mpesa.Kv `json:"ReferenceItem"`
}

// ResultData is the root parameter that encloses the entire result message.
type ResultData struct {

	// ID is a unique M-PESA transaction ID for every payment request. Same
	// value is sent to customer over SMS upon successful processing e.g. LHG31AA5TX.
	//
	// Required.
	ID string `json:"TransactionID" validate:"len=10,alphanum,uppercase"`

	// CID is a global unique identifier for the transaction request returned
	// by the M-PESA upon successful request submission e.g. 236543-276372-2.
	//
	// Required.
	CID string `json:"ConversationID" validate:"required,printascii,max=256"`

	// OCID is a global unique identifier for the transaction request returned
	// by the API proxy upon successful request submission e.g. AG_2376487236_126732989KJHJKH.
	//
	// Required.
	OCID string `json:"OriginatorConversationID" validate:"required,printascii,max=256"`

	// Type is a status code that indicates whether the transaction was already
	// sent to your listener. Usual value is 0.
	//
	// Required.
	Type int `json:"ResultType" validate:"min=0"`

	// Code is a numeric status code that indicates the status of the transaction
	// processing.
	//
	// 0 means success and any other code means an error occurred or the transaction
	// failed. Possible values are 0 and 2001.
	//
	// Required.
	Code int `json:"ResultCode" validate:"min=0"`

	// Desc is a message from the API that gives the status of the request
	// processing and usually maps to a specific result code value.
	//
	// Required.
	Desc string `json:"ResultDesc" validate:"required,max=2048"`

	// Reference is JSON object that holds more details about the result.
	//
	// Optional.
	Reference ReferenceData `json:"ReferenceData"`

	// Parameters is a JSON object that holds more details for the transaction.
	//
	// Parameters is only present for successful payments.
	//
	// Optional.
	Parameters mpesa.Parameters `json:"ResultParameters"`
}

// Result is a transaction status query result sent by M-Pesa to your ResultURL
// provided during status query request submission.
type Result struct {

	// Result is the root parameter that encloses the entire result message.
	Result ResultData `json:"Result"`
}

// CID returns the conversation ID of transaction status query request.
func (r *Result) CID() string {
	return r.Result.CID
}

// OCID returns the originator conversation ID  of transaction status query
// request.
func (r *Result) OCID() string {
	return r.Result.OCID
}

// ResultType returns the type of the result.
//
// Result type is a status code that indicates whether the transaction was already
// sent to your listener. Usual value is 0.
func (r *Result) ResultType() int {
	return r.Result.Type
}

// ResultCode returns status code of the transaction query request.
func (r *Result) ResultCode() int {
	return r.Result.Code
}

// ResultDesc returns the status description of the transaction query request.
func (r *Result) ResultDesc() string {
	return r.Result.Desc
}

// Successful returns true if the transaction status query was successful, else, false.
//
// NB: a successful transaction status query doesn't mean the payment was successful,
// please use Status(), StatusCompleted() or StatusCancelled() to determine the
// actual status of the payment.
func (r *Result) Successful() bool {
	return r.Result.Code == mpesa.ResultCodeSuccess
}

// Status returns the status of the payment.
func (r *Result) Status() (string, error) {
	v, err := mpesa.Value(ParameterKeyTransactionStatus, r.Result.Parameters.Parameter)
	if err != nil {
		return "", err
	}

	s, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ParameterKeyTransactionStatus, v)
	}

	return s, nil
}

// StatusCompleted returns true if the transaction status is 'Completed', else, false.
func (r *Result) StatusCompleted() (bool, error) {
	s, err := r.Status()
	if err != nil {
		return false, err
	}

	return s == TransactionStatusCompleted, nil
}

// StatusCancelled returns true if the transaction status is 'Cancelled', else, false.
func (r *Result) StatusCancelled() (bool, error) {
	s, err := r.Status()
	if err != nil {
		return false, err
	}

	return s == TransactionStatusCancelled, nil
}

// Amount returns the M-Pesa transaction amount.
func (r *Result) Amount() (float64, error) {
	v, err := mpesa.Value(ParameterKeyAmount, r.Result.Parameters.Parameter)
	if err != nil {
		return 0, err
	}

	switch amt := v.(type) {
	case float64:
		return amt, nil
	case int:
		return float64(amt), nil
	default:
		return 0, mpesa.InvalidParameterValueError(ParameterKeyAmount, v)
	}
}

// DebitPartyCharges returns the charges paid by the sender.
func (r *Result) DebitPartyCharges() (float64, error) {
	v, err := mpesa.Value(ParameterKeyDebitPartyCharges, r.Result.Parameters.Parameter)
	if err != nil {
		return 0, err
	}

	switch amt := v.(type) {
	case float64:
		return amt, nil
	case int:
		return float64(amt), nil
	default:
		return 0, mpesa.InvalidParameterValueError(ParameterKeyDebitPartyCharges, v)
	}
}

// Receipt returns a unique M-PESA transaction ID.
func (r *Result) Receipt() (string, error) {
	v, err := mpesa.Value(ParameterKeyReceiptNo, r.Result.Parameters.Parameter)
	if err != nil {
		return "", err
	}

	id, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ParameterKeyReceiptNo, v)
	}

	return id, nil
}

// DebitAccountType returns the account type of the sender.
func (r *Result) DebitAccountType() (string, error) {
	v, err := mpesa.Value(ParameterKeyDebitAccountType, r.Result.Parameters.Parameter)
	if err != nil {
		return "", err
	}

	dat, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ParameterKeyDebitAccountType, v)
	}

	return dat, nil
}

// OriginalOCID returns the original OriginatorConversationID.
func (r *Result) OriginalOCID() (string, error) {
	v, err := mpesa.Value(ParameterKeyOriginatorConversationID, r.Result.Parameters.Parameter)
	if err != nil {
		return "", err
	}

	ocid, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ParameterKeyOriginatorConversationID, v)
	}

	return ocid, nil
}

// OriginalCID returns the original ConversationID.
func (r *Result) OriginalCID() (string, error) {
	v, err := mpesa.Value(ParameterKeyConversationID, r.Result.Parameters.Parameter)
	if err != nil {
		return "", err
	}

	cid, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ParameterKeyConversationID, v)
	}

	return cid, nil
}

// DebitParty returns the shortcode and name of the sender.
func (r *Result) DebitParty() (*mpesa.Party, string, error) {
	v, err := mpesa.Value(ParameterKeyDebitPartyName, r.Result.Parameters.Parameter)
	if err != nil {
		return nil, "", err
	}

	name, ok := v.(string)
	if !ok {
		return nil, "", mpesa.InvalidParameterValueError(ParameterKeyDebitPartyName, v)
	}

	c, ok := mpesa.NewParty(name)
	if !ok {
		return nil, name, nil
	}

	return c, name, nil
}

// CreditParty returns the shortcode and name of the recipient.
func (r *Result) CreditParty() (*mpesa.Party, string, error) {
	v, err := mpesa.Value(ParameterKeyCreditPartyName, r.Result.Parameters.Parameter)
	if err != nil {
		return nil, "", err
	}

	name, ok := v.(string)
	if !ok {
		return nil, "", mpesa.InvalidParameterValueError(ParameterKeyCreditPartyName, v)
	}

	c, ok := mpesa.NewParty(name)
	if !ok {
		return nil, name, nil
	}

	return c, name, nil
}

// String returns a text format of Result by marshalling it into a JSON object.
func (r *Result) String() string {
	v, err := json.Marshal(r)
	if err != nil {
		return "{}"
	}

	return string(v)
}
