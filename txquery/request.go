package txquery

// Request is the request body sent to M-Pesa to check the status of a transaction.
type Request struct {

	// ID is a unique identifier to identify a transaction on Mpesa.
	//
	// Optional.
	ID string `json:"TransactionID,omitempty" validate:"uppercase,alphanum,len=10"`

	// OCID is the originator conversation ID issued to you by M-Pesa on successful
	// request submission.
	//
	// If you are querying the status of an STK push payment, this is going to be the
	// merchant request ID returned by the M-Pesa Express API.
	//
	// Optional.
	OCID string `json:"OriginalConversationID,omitempty" validate:"printascii,max=256"`

	// Initiator is the name of the initiator initiating the request.
	//
	// Required.
	Initiator string `json:"Initiator" validate:"required,max=256"`

	// Credential is the encrypted credential of the user getting transaction status.
	//
	// Required.
	Credential string `json:"SecurityCredential" validate:"base64"`

	// CommandID is a unique command that specifies M-Pesa transaction type.
	//
	// Takes only the 'TransactionStatusQuery' Command ID.
	//
	// Required.
	CommandID string `json:"CommandID" validate:"eq=TransactionStatusQuery"`

	// PartyA is a Organization receiving the transaction status query request.
	//
	// This will always be an organization shortcode.
	//
	// Required.
	PartyA string `json:"PartyA" validate:"numeric,min=5,max=12"`

	// IdentifierType is the type of organization receiving the transaction.
	//
	// This will always be 4 (Organization shortcode).
	//
	// Required.
	IdentifierType int `json:"IdentifierType" validate:"oneof=1 2 4"`

	// Remarks are comments that are sent along with the transaction.
	//
	// Remarks is optional and can only be 100 characters long or less.
	//
	// Optional.
	Remarks string `json:"Remarks,omitempty" validate:"max=100"`

	// Occassion, just like Remarks, is any additional information to be associated
	// with the transaction. Occassion is optional and can only be 100 characters
	// long or less.
	//
	// Optional.
	Occassion string `json:"Occassion,omitempty" validate:"max=100"`

	// ResultURL is the path that stores information of a transaction.
	//
	// Required.
	ResultURL string `json:"ResultURL" validate:"http_url"`

	// QueueTimeoutURL is the path that stores information of timeout transaction.
	//
	// Required.
	QueueTimeoutURL string `json:"QueueTimeOutURL" validate:"http_url"`
}

// IDQuery is a data transfer object for transaction status query by M-Pesa
// Transaction ID.
type IDQuery struct {

	// ID is a unique identifier to identify a transaction on Mpesa.
	//
	// Optional.
	ID string `validate:"uppercase,alphanum,len=10"`

	// Remarks are comments that are sent along with the transaction.
	//
	// Remarks is optional and can only be 100 characters long or less.
	//
	// Optional.
	Remarks string `validate:"required,min=2,max=100"`

	// Occassion, just like Remarks, is any additional information to be associated
	// with the transaction. Occassion is optional and can only be 100 characters
	// long or less.
	//
	// Optional.
	Occassion string `validate:"max=100"`

	// ResultURL is the path that stores information of a transaction.
	//
	// Required.
	ResultURL string `validate:"http_url"`

	// QueueTimeoutURL is the path that stores information of timeout transaction.
	//
	// Required.
	QueueTimeoutURL string `validate:"http_url"`
}

// OCIDQuery is a data transfer object for transaction status query by M-Pesa
// originator conversation ID.
type OCIDQuery struct {

	// OCID is a global unique identifier for the transaction request returned by
	// the API proxy upon successful request submission. If you don’t have the
	// M-PESA transaction ID you can use this to query.
	//
	// Optional.
	OCID string `validate:"printascii,min=2,max=256"`

	// Remarks are comments that are sent along with the transaction.
	//
	// Remarks is optional and can only be 100 characters long or less.
	//
	// Optional.
	Remarks string `validate:"min=2,max=100"`

	// Occassion, just like Remarks, is any additional information to be associated
	// with the transaction. Occassion is optional and can only be 100 characters
	// long or less.
	//
	// Optional.
	Occassion string `validate:"max=100"`

	// ResultURL is the path that stores information of a transaction.
	//
	// Required.
	ResultURL string `validate:"http_url"`

	// QueueTimeoutURL is the path that stores information of timeout transaction.
	//
	// Required.
	QueueTimeoutURL string `validate:"http_url"`
}
