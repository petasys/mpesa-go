package mpesa

import "time"

// MPesaStatementTimeFormat is a widely used timestamp format on M-Pesa statements/reports.
//
// NB: This is subject to change.
const MPesaStatementTimeFormat = "02-01-2006 15:04:05"

// EATZone is the Africa/Nairobi timezone.
var EATZone = time.FixedZone("EAT", int((time.Hour * 3).Seconds()))

// NowString returns the current date and time (in the EAT timezone) in a format
// used on most M-Pesa statements/reports.
func NowString() string {
	return time.Now().In(EATZone).Format(MPesaStatementTimeFormat)
}
