package b2b

import "encoding/json"

// Request is a definition of the HTTP request body fomart for the Safaricom's
// Daraja B2B RESTful API.
type Request struct {

	// Initiator is the username for a M-Pesa portal user with the B2B API Initiator
	// role.
	//
	// Required.
	Initiator string `json:"Initiator" validate:"required,max=256"`

	// Credential is the value obtained after encrypting the API initiator
	// password.
	//
	// Required.
	Credential string `json:"SecurityCredential" validate:"base64"`

	// CommandID is a unique command that specifies B2B transaction type.
	//
	// The following command IDs are supported:
	//
	// 1. BusinessPayBill: The transaction moves money from your MMF/Working account
	// to the recipient’s utility account.
	//
	// 2. BusinessBuyGoods: The transaction moves money from your MMF/Working account
	// to the recipient’s merchant account.
	//
	// Required.
	CommandID string `json:"CommandID" validate:"oneof=BusinessPayBill BusinessBuyGoods"`

	// PartyA is the shortcode from which money will be deducted.
	//
	// Required.
	PartyA string `json:"PartyA" validate:"numeric,min=5,max=16"`

	// SenderIDType is the type of shortcode from which money is deducted.
	//
	// It's always 4 (Organization short code).
	//
	// Required.
	SenderIDType int `json:"SenderIdentifierType" validate:"oneof=2 4"`

	// PartyB is the shortcode to which money will be moved.
	//
	// This can be a Till number or an organization shortcode.
	//
	// Required.
	PartyB string `json:"PartyB" validate:"numeric,min=5,max=16"`

	// RecieverIDType is the type of shortcode to which money is credited.
	//
	// Use 2 if your PartyB is a Till Number, else, use 4 (Organization shortcode).
	//
	// Required.
	RecieverIDType int `json:"RecieverIdentifierType" validate:"oneof=2 4"`

	// Amount is the transaction amount.
	//
	// Required.
	Amount float64 `json:"Amount" validate:"min=1,max=10000000"`

	// Requester is the consumer’s mobile number on behalf of whom you are paying.
	//
	// Optional.
	Requester string `json:"Requester,omitempty"`

	// Ref is the account number to be associated with the payment.
	//
	// Required.
	Ref string `json:"AccountReference" validate:"min=1,max=13"`

	// Remarks is any additional information to be associated with the transaction.
	//
	// Remarks can only be 100 characters long or less.
	//
	// Required.
	Remarks string `json:"Remarks" validate:"min=2,max=100"`

	// Occassion, just like Remarks, is any additional information to be associated
	// with the transaction.
	//
	// Occassion is optional and can only be 100 characters long or less.
	//
	// Optional.
	Occassion string `json:"Occassion,omitempty" validate:"max=100"`

	// ResultURL is a URL that will be used to send transaction results after processing.
	//
	// Required.
	ResultURL string `json:"ResultURL" validate:"http_url"`

	// QueueTimeoutURL is a URL that will be used to notify your system in case
	// the request times out before processing.
	//
	// Required.
	QueueTimeoutURL string `json:"QueueTimeOutURL" validate:"http_url"`
}

// B2BInfo holds required and/or optional payment data expected from the developer
// by the library.
//
// The library sends data in b2b.Request by automatically adding the security credentials
// and the sender shortcode info.
type B2BInfo struct {

	// PartyB is the shortcode to which money will be moved.
	//
	// This will be either a Till number or an organization shortcode.
	//
	// Required.
	PartyB string `validate:"numeric,min=5,max=16"`

	// Amount is the transaction amount.
	//
	// Required.
	Amount float64 `validate:"min=1,max=10000000"`

	// Requester is the consumer’s mobile number on behalf of whom you are paying.
	//
	// Optional.
	Requester string

	// Ref is the account number to be associated with the payment.
	//
	// Required.
	Ref string `validate:"min=1,max=13"`

	// Remarks is any additional information to be associated with the transaction.
	//
	// Remarks can only be 100 characters long or less.
	//
	// Required.
	Remarks string `validate:"min=2,max=100"`

	// Occassion, just like Remarks, is any additional information to be associated
	// with the transaction.
	//
	// Occassion is optional and can only be 100 characters long or less.
	//
	// Optional.
	Occassion string `validate:"max=100"`

	// ResultURL is a URL that will be used to send transaction results after processing.
	//
	// Required.
	ResultURL string `validate:"http_url"`

	// QueueTimeoutURL is a URL that will be used to notify your system in case
	// the request times out before processing.
	//
	// Required.
	QueueTimeoutURL string `validate:"http_url"`
}

// String returns a text format of Request by marshalling it into a JSON object.
func (r *Request) String() string {
	v, err := json.Marshal(r)
	if err != nil {
		return "{}"
	}

	return string(v)
}
