package b2b

import (
	"encoding/json"

	"gitlab.com/petasys/mpesa-go"
)

const (

	// ParameterKeyCurrency is a currency code of the transaction amount.
	//
	// Returned as one of the key-value objects under the ResultParameter array.
	ParameterKeyCurrency = "Currency"

	// ParameterKeyAmount is the amount that is transacted e.g. 100.
	//
	// It is also returned under the ResultParameter array.
	ParameterKeyAmount = "Amount"

	// ParameterKeyDebitPartyCharges is the Transaction fee deducted on the debit
	// party if applicable.
	//
	// The value is empty if no charges apply.
	//
	// This parameter is returned as one of the key-value objects under the
	// ResultParameter array.
	ParameterKeyDebitPartyCharges = "DebitPartyCharges"

	// ParameterKeyDebitPartyAffectedAccountBalance is the balance in the organization's
	// account from which funds were deducted under the shortcode.
	//
	// Returned as one of the key-value objects under the ResultParameter array.
	ParameterKeyDebitPartyAffectedAccountBalance = "DebitPartyAffectedAccountBalance"

	// ParameterKeyDebitAccountCurrentBalance is the balance in the organization's
	// account from which funds were deducted under the shortcode.
	//
	// Returned as one of the key-value objects under the ResultParameter array.
	ParameterKeyDebitAccountCurrentBalance = "DebitAccountCurrentBalance"

	// ParameterKeyInitiatorAccountCurrentBalance is the balance in the organization
	// accounts from which funds were deducted under the shortcode.
	//
	// Returned as one of the key-value objects under the ResultParameter array.
	ParameterKeyInitiatorAccountCurrentBalance = "InitiatorAccountCurrentBalance"

	// ParameterKeyTransCompletedTime is a 14-digit timestamp indicating the date
	// and time the transaction completed on M-PESA.
	//
	// Returned as one of the key-value objects under the ResultParameter array.
	ParameterKeyTransCompletedTime = "TransCompletedTime"

	// ParameterKeyReceiverPartyPublicName is the public name of the credit
	// party/organization.
	//
	// Returned as one of the key-value objects under the ResultParameter array.
	ParameterKeyReceiverPartyPublicName = "ReceiverPartyPublicName"

	// ItemKeyReceiverBillReferenceNumber is the account number to be associated
	// with the payment.
	//
	// Returned as one of the key-value objects under the ReferenceItem array.
	ItemKeyReceiverBillReferenceNumber = "BillReferenceNumber"
)

// Reference is JSON object that holds more details about the result.
type ReferenceData struct {
	Item []mpesa.Kv `json:"ReferenceItem"`
}

// ResultData is the root parameter that encloses the entire result message.
type ResultData struct {

	// ID is a unique M-PESA transaction ID for every payment request. Same
	// value is sent to customer over SMS upon successful processing e.g. LHG31AA5TX.
	//
	// Required.
	ID string `json:"TransactionID" validate:"len=10,alphanum,uppercase"`

	// CID is a global unique identifier for the transaction request returned
	// by the M-PESA upon successful request submission e.g. 236543-276372-2.
	//
	// Required.
	CID string `json:"ConversationID" validate:"required,printascii,max=256"`

	// OCID is a global unique identifier for the transaction request returned
	// by the API proxy upon successful request submission e.g. AG_2376487236_126732989KJHJKH.
	//
	// Required.
	OCID string `json:"OriginatorConversationID" validate:"required,printascii,max=256"`

	// Type is a status code that indicates whether the transaction was already
	// sent to your listener. Usual value is 0.
	//
	// Required.
	Type int `json:"ResultType" validate:"min=0"`

	// Code is a numeric status code that indicates the status of the transaction
	// processing.
	//
	// 0 means success and any other code means an error occurred or the transaction
	// failed. Possible values are 0 and 2001.
	//
	// Required.
	Code int `json:"ResultCode" validate:"min=0"`

	// Desc is a message from the API that gives the status of the request
	// processing and usually maps to a specific result code value.
	//
	// Required.
	Desc string `json:"ResultDesc" validate:"required,max=2048"`

	// Reference is JSON object that holds more details about the result.
	//
	// Optional.
	Reference ReferenceData `json:"ReferenceData"`

	// Parameters is a JSON object that holds more details for the transaction.
	//
	// Parameters is only present for successful payments.
	//
	// Optional.
	Parameters mpesa.Parameters `json:"ResultParameters"`
}

// Result is a HTTP request body sent by M-Pesa to your ResultURL on success or fail.
type Result struct {

	// Result is the root parameter that encloses the entire result message.
	Result ResultData `json:"Result"`
}

// ID returns a unique M-PESA transaction ID.
func (r *Result) ID() string {
	return r.Result.ID
}

// CID returns the transaction's conversation ID.
func (r *Result) CID() string {
	return r.Result.CID
}

// OCID returns the transaction's originator conversation ID.
func (r *Result) OCID() string {
	return r.Result.OCID
}

// ResultType returns the type of the result.
//
// Result type is a status code that indicates whether the transaction was already
// sent to your listener. Usual value is 0.
func (r *Result) ResultType() int {
	return r.Result.Type
}

// ResultCode returns the transaction's result status code.
//
// Result status code is a numeric status code that indicates the status of the
// transaction processing.
func (r *Result) ResultCode() int {
	return r.Result.Code
}

// ResultDesc returns the
func (r *Result) ResultDesc() string {
	return r.Result.Desc
}

// Successful determines whether the transaction was successful or not.
func (r *Result) Successful() bool {
	return r.Result.Code == mpesa.ResultCodeSuccess
}

// Amount returns the M-Pesa transaction amount.
func (r *Result) Amount() (float64, error) {
	v, err := mpesa.Value(ParameterKeyAmount, r.Result.Parameters.Parameter)
	if err != nil {
		return 0, err
	}

	switch amt := v.(type) {
	case float64:
		return amt, nil
	case int:
		return float64(amt), nil
	default:
		return 0, mpesa.InvalidParameterValueError(ParameterKeyAmount, v)
	}
}

// DebitPartyCharges returns the Transaction fee deducted on the debit party if
// applicable.
func (r *Result) DebitPartyCharges() (float64, error) {
	v, err := mpesa.Value(ParameterKeyDebitPartyCharges, r.Result.Parameters.Parameter)
	if err != nil {
		return 0, err
	}

	if v == nil {
		return 0, nil
	}

	switch amt := v.(type) {
	case float64:
		return amt, nil
	case int:
		return float64(amt), nil
	default:
		return 0, mpesa.InvalidParameterValueError(ParameterKeyDebitPartyCharges, v)
	}
}

// TransCompletedTime returns the date and time the transaction was completed.
func (r *Result) TransCompletedTime() (string, error) {
	v, err := mpesa.Value(ParameterKeyTransCompletedTime, r.Result.Parameters.Parameter)
	if err != nil {
		return "", err
	}

	dt, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ParameterKeyTransCompletedTime, v)
	}

	return dt, nil
}

// ReceiverPartyPublicName returns the recipients shortcode and full name.
func (r *Result) ReceiverPartyPublicName() (*mpesa.Party, string, error) {
	v, err := mpesa.Value(ParameterKeyReceiverPartyPublicName, r.Result.Parameters.Parameter)
	if err != nil {
		return nil, "", err
	}

	name, ok := v.(string)
	if !ok {
		return nil, "", mpesa.InvalidParameterValueError(ParameterKeyReceiverPartyPublicName, v)
	}

	c, ok := mpesa.NewParty(name)
	if !ok {
		return nil, name, nil
	}

	return c, name, nil
}

// DebitPartyAffectedAccountBalance returns the balance in the organization's account
// from which funds were deducted under the shortcode.
func (r *Result) DebitPartyAffectedAccountBalance() (string, error) {
	v, err := mpesa.Value(ParameterKeyDebitPartyAffectedAccountBalance, r.Result.Parameters.Parameter)
	if err != nil {
		return "", err
	}

	bal, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ParameterKeyDebitPartyAffectedAccountBalance, v)
	}

	return bal, nil
}

// DebitAccountCurrentBalance returns the balance in the organization's account
// from which funds were deducted under the shortcode.
func (r *Result) DebitAccountCurrentBalance() (*mpesa.Amount, string, error) {
	v, err := mpesa.Value(ParameterKeyDebitAccountCurrentBalance, r.Result.Parameters.Parameter)
	if err != nil {
		return nil, "", err
	}

	bal, ok := v.(string)
	if !ok {
		return nil, "", mpesa.InvalidParameterValueError(ParameterKeyDebitAccountCurrentBalance, v)
	}

	amt, err := mpesa.NewAmount(bal)
	return amt, bal, err
}

// InitiatorAccountCurrentBalance returns the balance in the organization accounts
// from which funds were deducted under the shortcode.
func (r *Result) InitiatorAccountCurrentBalance() (*mpesa.Amount, string, error) {
	v, err := mpesa.Value(ParameterKeyInitiatorAccountCurrentBalance, r.Result.Parameters.Parameter)
	if err != nil {
		return nil, "", err
	}

	bal, ok := v.(string)
	if !ok {
		return nil, "", mpesa.InvalidParameterValueError(ParameterKeyInitiatorAccountCurrentBalance, v)
	}

	amt, err := mpesa.NewAmount(bal)
	return amt, bal, err
}

// BillReferenceNumber returns the account number to be associated with the payment.
func (r *Result) BillReferenceNumber() (string, error) {
	v, err := mpesa.Value(ItemKeyReceiverBillReferenceNumber, r.Result.Reference.Item)
	if err != nil {
		return "", err
	}

	billRef, ok := v.(string)
	if !ok {
		return "", mpesa.InvalidParameterValueError(ItemKeyReceiverBillReferenceNumber, v)
	}

	return billRef, nil
}

// String returns a text format of Result by marshalling it into a JSON object.
func (r *Result) String() string {
	v, err := json.Marshal(r)
	if err != nil {
		return "{}"
	}

	return string(v)
}
