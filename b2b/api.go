// Copyright 2023 M-Pesa Go Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// The B2B API enables you to:
//
// 1. Pay bills directly from your business account to a pay bill number, or a
// paybill store.
//
// 1.1. You can use this API to pay on behalf of a consumer/requester.
//
// 1.2. The transaction moves money from your MMF/Working account to the recipient’s
// utility account.
//
// 2. Pay for goods and services directly from your business account to a till
// number, merchant store number or Merchant HO.
//
// 2.1. You can also use this API to pay a merchant on behalf of a consumer/requestor.
//
// 2.2. The transaction moves money from your MMF/Working account to the recipient’s
// merchant account.
package b2b

import (
	"bytes"
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"github.com/go-playground/validator/v10"
	"gitlab.com/petasys/mpesa-go"
)

// API provides utility functions that implement B2B payments via M-Pesa's Daraja
// B2B API.
type API struct {

	// env is the B2B API host name.
	env string

	// client is the underlying HTTP client with support for interceptors e.t.c.
	client mpesa.Client

	// tknStore is a store where the access token is retrived for each transaction.
	tknStore mpesa.TokenStore

	// userStore is a store where the M-Pesa initiator name and password are stored.
	userStore mpesa.InitiatorStore

	// encryptor is an API for encrypting initiator password using RSA pub key and
	// encoding the result using base64.
	encryptor mpesa.SCEncryptor

	// validate is used to run validation checks for all request params. The idea is to
	// cancel an invalid request before it makes it to M-Pesa.
	validate *validator.Validate
}

// send prepares the actual http.Request and forwards it to M-Pesa.
func (api *API) send(t mpesa.Token, b *Request) (*mpesa.Response, error) {
	bBytes, err := json.Marshal(&b)
	if err != nil {
		return nil, mpesa.NewAPIError(true, err, "b2b.Request to JSON conversion failed")
	}

	url := api.env + mpesa.PathB2B
	req, err := http.NewRequest(http.MethodPost, url, bytes.NewReader(bBytes))
	if err != nil {
		return nil, mpesa.NewAPIError(true, err, "Creating a new http.Request failed")
	}

	req.Header.Set("Authorization", "Bearer "+t.Token())
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Accept", "application/json")

	res, err := api.client.Post(req)
	if err != nil {
		return nil, mpesa.NewAPIError(false, err, "Network error occurred")
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		var errorBody mpesa.ErrorResponse
		if err := json.NewDecoder(res.Body).Decode(&errorBody); err != nil {
			return nil, mpesa.NewAPIError(false, err, "JSON to mpesa.ErrorResponse conversion failed")
		}

		return nil, &errorBody
	}

	var okBody mpesa.Response
	if err := json.NewDecoder(res.Body).Decode(&okBody); err != nil {
		return nil, mpesa.NewAPIError(false, err, "JSON to mpesa.Response conversion failed")
	}

	return &okBody, nil
}

// PayBill moves money from your MMF/Working account to the recipient’s utility account.
func (api *API) PayBill(shortcode string, info *B2BInfo) (*Request, *mpesa.Response, error) {
	if err := api.validate.Struct(info); err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "b2b.B2BInfo is invalid")
	}

	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Token not found for shortcode "+shortcode)
	}

	user, err := api.userStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator not found for shortcode "+shortcode)
	}

	pwd, err := api.encryptor.Encrypt(user.Password())
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator password encryption failed")
	}

	b := Request{
		CommandID:       mpesa.CommandIDBusinessPayBill,
		Initiator:       user.Name(),
		Credential:      pwd,
		PartyA:          shortcode,
		SenderIDType:    mpesa.IdentifierTypeOrgShortcode,
		PartyB:          info.PartyB,
		RecieverIDType:  mpesa.IdentifierTypeOrgShortcode,
		Amount:          info.Amount,
		Ref:             info.Ref,
		Requester:       info.Requester,
		Remarks:         info.Remarks,
		Occassion:       info.Occassion,
		ResultURL:       info.ResultURL,
		QueueTimeoutURL: info.QueueTimeoutURL,
	}

	res, err := api.send(tkn, &b)
	return &b, res, err
}

// BuyGoods moves money from your MMF/Working account to the recipient’s merchant account.
func (api *API) BuyGoods(shortcode string, info *B2BInfo) (*Request, *mpesa.Response, error) {
	if err := api.validate.Struct(info); err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "b2b.B2BInfo is invalid")
	}

	tkn, err := api.tknStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Token not found for shortcode "+shortcode)
	}

	init, err := api.userStore.Read(shortcode)
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator not found for shortcode "+shortcode)
	}

	pwd, err := api.encryptor.Encrypt(init.Password())
	if err != nil {
		return nil, nil, mpesa.NewAPIError(true, err, "Initiator password encryption failed")
	}

	b := Request{
		CommandID:       mpesa.CommandIDBusinessBuyGoods,
		Initiator:       init.Name(),
		Credential:      pwd,
		PartyA:          shortcode,
		SenderIDType:    mpesa.IdentifierTypeOrgShortcode,
		PartyB:          info.PartyB,
		RecieverIDType:  mpesa.IdentifierTypeTillNumber,
		Amount:          info.Amount,
		Ref:             info.Ref,
		Requester:       info.Requester,
		Remarks:         info.Remarks,
		Occassion:       info.Occassion,
		ResultURL:       info.ResultURL,
		QueueTimeoutURL: info.QueueTimeoutURL,
	}

	res, err := api.send(tkn, &b)
	return &b, res, err
}

// New creates a new instance of API.
func New(env string, clt mpesa.Client, ts mpesa.TokenStore, is mpesa.InitiatorStore, v *validator.Validate,
	enc mpesa.SCEncryptor) (*API, error) {

	if len(env) == 0 {
		return nil, errors.New("env is blank")
	}

	if clt == nil {
		return nil, errors.New("client is Nil")
	}

	if ts == nil {
		return nil, errors.New("token store is Nil")
	}

	if is == nil {
		return nil, errors.New("initiator store is Nil")
	}

	if v == nil {
		return nil, errors.New("validator is Nil")
	}

	if enc == nil {
		return nil, errors.New("security credentials encryptor is Nil")
	}

	return &API{env: strings.Trim(env, "/"), client: clt, tknStore: ts, userStore: is, validate: v, encryptor: enc}, nil
}
