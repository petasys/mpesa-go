package mpesa

const (
	VersionMajor = 1
	VersionMinor = 7
	VersionPatch = 0

	Version     = "1.7.0"
	LibraryName = "mpesa-go/1.7.0"
)
